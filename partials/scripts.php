<!-- Bootstrap core JavaScript-->
<script src="<?php print SERVER_URL ?>/views/vendor/jquery/jquery.min.js"></script>
<script src="<?php print SERVER_URL ?>/views/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Core plugin JavaScript-->
<script src="<?php print SERVER_URL ?>/views/vendor/jquery-easing/jquery.easing.min.js"></script>
<!-- Custom scripts for all pages-->
<script src="<?php print SERVER_URL ?>/views/js/sb-admin-2.min.js"></script>
<!-- Page level plugins -->
<script src="vendor/chart.js/Chart.min.js"></script>
<!-- Page level custom scripts -->
<script src="<?php print SERVER_URL ?>/views/js/demo/chart-area-demo.js"></script>
<script src="<?php print SERVER_URL ?>/views/js/demo/chart-pie-demo.js"></script>