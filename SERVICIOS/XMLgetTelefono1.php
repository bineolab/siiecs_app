<?php
header("Content-type: text/xml");  // Algo Nuevo y es la regla numero 1

$servername = "localhost";
$username = "root"; 
$password = "";
$dbname ="sistemadeinventario";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Conexión Fallida: " . $conn->connect_error);
} 

$sql = "SELECT  Numero, IdTipoTelefono, Activo from telefono where IdTelefono='".$_GET["idTelefono1"]."'";

$result = $conn->query($sql);

$xml_output = "<?xml version=\"1.0\"?>\n";  
$xml_output .= "<tel>\n"; 

if ($result->num_rows > 0) {

    while($row = $result->fetch_assoc()) {
        $xml_output .= "\t<item>\n"; 
        $xml_output .= "\t\t<numeroA>" . $row['Numero'] . "</numeroA>\n";
        $xml_output .= "\t\t<idTipoTelefonoA>" . $row['IdTipoTelefono'] . "</idTipoTelefonoA>\n";
        $xml_output .= "\t\t<activoA>" . $row['Activo'] . "</activoA>\n";
		$xml_output .= "\t</item>\n"; 
    }
} 

$xml_output .= "</tel>"; 

echo $xml_output; 
?>