<?php
header("Content-type: text/xml");  // Algo Nuevo y es la regla numero 1

$servername = "localhost";
$username = "root"; 
$password = "";
$dbname ="sistemadeinventario";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Conexión Fallida: " . $conn->connect_error);
} 

$sql = "SELECT CodigoSeccion, Activo from seccion
where IdSeccion='".$_GET["IdSeccion"]."'";

$result = $conn->query($sql);

$xml_output = "<?xml version=\"1.0\"?>\n";  
$xml_output .= "<seccion>\n"; 

if ($result->num_rows > 0) {

    while($row = $result->fetch_assoc()) {
        $xml_output .= "\t<item>\n"; 
        $xml_output .= "\t\t<CodigoSeccion>" . $row['CodigoSeccion'] . "</CodigoSeccion>\n";
        $xml_output .= "\t\t<Activo>" . $row['Activo'] . "</Activo>\n";
		$xml_output .= "\t</item>\n"; 
    }
} 
$xml_output .= "</seccion>"; 

echo $xml_output; 
?>