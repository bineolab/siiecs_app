<?php

header("Content-type: text/xml"); 
$servername = "localhost";
$username = "root"; 
$password = "";
$database = "sistemadeinventario";
$conn = new mysqli($servername, $username, $password,$database);

if ($conn->connect_error) {
    die("Conexión Fallida: " . $conn->connect_error);
} 

$sql = "SELECT Numero, IdTipoTelefono, NumeroTH, Activo FROM telefono WHERE NumeroTH =".$_GET["numeroTH"];
$result = $conn->query($sql); 

$xml = "<?xml version=\"1.0\"?>\n"; 
$xml .= "<telefonos>\n"; 
if($result->num_rows > 0){
    while($row = $result->fetch_assoc()){
        $xml .= "\t<telefono>\n"; 
		$xml .= "\t\t<IdTipoTelefono>". $row['IdTipoTelefono'] ."</IdTipoTelefono>\n"; 
        $xml .= "\t\t<Numero>" . $row['Numero'] . "</Numero>\n";
        $xml .= "\t\t<activo>" . $row['Activo'] . "</activo>\n";
        $xml .= "\t</telefono>\n"; 
    }
}
$xml .= "</telefonos>"; 
print $xml;
?>