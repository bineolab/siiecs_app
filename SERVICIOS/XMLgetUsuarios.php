<?php
header("Content-type: text/xml");  // Algo Nuevo y es la regla numero 1

$servername = "localhost";
$username = "root"; 
$password = "";
$dbname ="sistemadeinventario";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Conexión Fallida: " . $conn->connect_error);
} 

$sql = "SELECT Nombres, Apellidos, Correo, Contrasenia, Activo, IdTipoUsuario from usuarios where NumeroTH='".$_GET["numeroTH"]."'";

$result = $conn->query($sql);

$xml_output = "<?xml version=\"1.0\"?>\n";  
$xml_output .= "<usuarios>\n"; 

if ($result->num_rows > 0) {

    while($row = $result->fetch_assoc()) {
        $xml_output .= "\t<item>\n"; 
        
        $xml_output .= "\t\t<nombres>" . $row['Nombres'] . "</nombres>\n";
        $xml_output .= "\t\t<apellidos>" . $row['Apellidos'] . "</apellidos>\n";
        $xml_output .= "\t\t<correo>" . $row['Correo'] . "</correo>\n";
        $xml_output .= "\t\t<contrasenia>" . $row['Contrasenia'] . "</contrasenia>\n";
        $xml_output .= "\t\t<activo>" . $row['Activo'] . "</activo>\n";
        $xml_output .= "\t\t<idTipoUsuario>" . $row['IdTipoUsuario'] . "</idTipoUsuario>\n";
		$xml_output .= "\t</item>\n"; 
    }
} 

$xml_output .= "</usuarios>"; 

echo $xml_output; 
?>