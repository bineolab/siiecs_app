<?php
$conn = new mysqli( 'localhost', 'root', '', 'sistemadeinventario' );
if ( $conn->connect_error ) {
  die( 'Error al conectarse a la base de datos' );
}
$res = array( 'error' => false );
$action = '';
if ( isset( $_GET['action'] ) ) {
  $action = $_GET['action'];
}

switch($action){
  case 'getArticulos':
    $result = $conn->query( "SELECT IdArticulo, Descripcion FROM articulo" );
    $articulos = array();
    while ( $row = $result->fetch_assoc() ) {
      array_push( $articulos, $row );
    }
    $res['articulos'] = $articulos;
    break;
  
    case 'createM':
      $result = $conn->query( "INSERT INTO inventariomaestro (IdProveedor,NumFact,FechaCompra) 
                      VALUES (".$_POST['idProveedor'].", '".$_POST['numeroFactura']."', '". $_POST['fechaCompra']."')" );
      
      $last_id = mysqli_insert_id($conn);
      
      if ( $result ) {
        $res['msg'] = 'Inventario Maestro agregado con éxito.';
        $res['ultimoID'] = $last_id;
      } else {
        $res['error'] = true;
        $res['msg'] = "Error al tratar de agregar la factura maestra";
      }
      break;
  
    case 'createArticulo':
      $idMaestro =   $_GET["idMaestro"] ;
      $cantidad =   $_GET["cantidad"] ;
      $precio =  $_GET["precio"] ;
      $idArticulo =  $_GET["idArticulo"] ;
      $subTotal =  $_GET["subTotal"] ;
      $total = $_GET["total"] ;

      $sql = "INSERT INTO inventariodetalle (IdInventario,Cantidad, Precio, IdArticulo, SubTotal, Total) 
      VALUES (".$idMaestro.", ".$cantidad.", ".$precio.", ".$idArticulo.", ".$subTotal.", ".$total.")" ;
      $result = $conn->query( $sql );

      if ( $result ) {
        $res['msg'] = 'Detalles agregado con éxito.';
        $res['res'] = $result;
      } else {
        $res['error'] = true;
        $res['res'] = $result;
        $res['msg'] = "Error al tratar de agregar la factura del detalle ".$sql." - ".$conn->error;
      }
    break;

    case 'updateArticulo':
      break;

    default:
      break;
}

$conn->close();
header( 'Content-type: application/json' );
echo json_encode($res);
