<?php
$conn = new mysqli( 'localhost', 'root', '', 'sistemadeinventario' );
if ( $conn->connect_error ) {
  die( 'Error al conectarse a la base de datos' );
}
$res = array( 'error' => false );
$action = '';
if ( isset( $_GET['action'] ) ) {
  $action = $_GET['action'];
}

switch($action){
	case 'getSolicitudes':
		$sql = "SELECT 
		S.NombrePractica, 
		S.NumeroTH, U.Nombres, U.Apellidos, S.Fecha, S.IdClase, C.NombreClase, S.HoraInicio, S.HoraFin,
		S.Seccion, S.IdEstado, ES.Nombre , S.IdSolicitud, S.HoraClase, S.FechaSolicitud, U.Correo
		FROM solicitud S INNER JOIN usuarios U
		ON S.NumeroTH = U.NumeroTH
		INNER JOIN clases C 
		ON C.IdClase = S.IdClase
		inner JOIN estadosolicitud ES
		ON ES.IdEstado = S.IdEstado";
		$result = $conn->query( $sql );
		$articulos = array();
		while ( $row = $result->fetch_assoc() ) {
			array_push( $articulos, $row );
		}
		$res['solicitudes'] = $articulos;
		
		break;
	
	case 'getDetalleSolicitud':
		$sql = "SELECT DS.IdSolicitud, DS.IdArticulo, A.Descripcion AS NombreArticulo, 
		TA.Descripcion AS NombreTipoArticulo, DS.CantidadSolicitada, DS.CantidadDevolucion
		FROM detallesolicitud DS 
		INNER JOIN articulo A
		ON DS.IdArticulo = A.IdArticulo
		INNER JOIN tipoarticulo TA
		ON TA.IdTipoArticulo = A.IdTipoArticulo
		INNER JOIN solicitud S
		ON DS.IdSolicitud = ".$_GET['idsolicitud']." 
		AND DS.IdSolicitud = S.IdSolicitud";
		$result = $conn->query( $sql );
		if($result->num_rows > 0){
			$articulos = array();
			while ( $row = $result->fetch_assoc() ) {
				array_push( $articulos, $row );
			}
			$res['detalles'] = $articulos;
		}else{
			$res['error'] = true;
			$res['msg'] = "El docente no ingresó articulos a la solicitud...";
		}
		break;
	case 'enviarCorreo':
		$destino = $_GET['emailDocente'];
		$nombre = "SISTEMA SIIECS";
		$correo = "mensajes@siiecs.unitec.edu";
		$contenido = $_GET['mensaje'];
		$cabeceras = 'From: webmaster@example.com' . "\r\n" .
					'Reply-To: webmaster@example.com' . "\r\n" .
					'X-Mailer: PHP/' . phpversion();
		mail($destino,"Respuesta a su requisición",$contenido,$cabeceras);
		$res['msg'] = "Correo enviado exitosamente!\n".$contenido." - ".$destino;
		break;
	case 'cambiarEstado':
		$estado = "";
		switch($_GET['IdEstado']){
			case 2:
				$estado = "Aceptada";
				break;
			case 3:
				$estado = "Rechazada";
				break;
			case 4:
				$estado = "Entregada";
				break;
			case 5:
				$estado = "Finalizada";
				break;
			default:
				$estado = "ERROR";
		}
		$sql = "UPDATE solicitud SET IdEstado=".$_GET['IdEstado']." WHERE IdSolicitud=".$_GET['IdSolicitud'];
		$result = $conn->query( $sql );
		if($result){
			$res['error'] = false;
			$res['msg'] = "Solicitud actualizada como ".$estado;
		}else{
			$res['error'] = true;
			$res['msg'] = "Error al tratar de actualizar el estado de la sulicitud...\n".$conn->error;
 		}
		break;
	default:
		break;
}

$conn->close();
header( 'Content-Type: application/json' );
//echo $res['solicitudes'][0]['NombrePractica'];
echo json_encode($res);

// https://parzibyte.me/blog/2018/03/12/axios-php-ejemplo-peticiones-ajax/