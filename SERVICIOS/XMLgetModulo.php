<?php

header("Content-type: text/xml"); 
$servername = "localhost";
$username = "root"; 
$password = "";
$database = "sistemadeinventario";
$conn = new mysqli($servername, $username, $password,$database);

if ($conn->connect_error) {
    die("Conexión Fallida: " . $conn->connect_error);
} 

$sql = "SELECT Nombre, Activo from modulos where IdModulo='".$_GET["idmodulo"]."'";

$result = $conn->query($sql); 

$xml = "<?xml version=\"1.0\"?>\n"; 
$xml .= "<modulo>\n"; 
if($result->num_rows > 0){
    while($row = $result->fetch_assoc()){
        $xml .= "\t<item>\n"; 
		$xml .= "\t\t<nombre>" . $row['Nombre'] . "</nombre>\n";
        $xml .= "\t\t<activo>" . $row['Activo'] . "</activo>\n";
        $xml .= "\t</item>\n"; 
    }
}
$xml .= "</modulo>"; 
print $xml;
?>