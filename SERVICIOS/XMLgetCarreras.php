<?php
header("Content-type: text/xml");  // Algo Nuevo y es la regla numero 1

$servername = "localhost";
$username = "root"; 
$password = "";
$dbname ="sistemadeinventario";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Conexión Fallida: " . $conn->connect_error);
} 

$sql = "SELECT NombreCarrera, Activo from Carreras where IdCarrera='".$_GET["idCarrera"]."'";

$result = $conn->query($sql);

$xml_output = "<?xml version=\"1.0\"?>\n";  
$xml_output .= "<carrera>\n"; 

if ($result->num_rows > 0) {

    while($row = $result->fetch_assoc()) {
        $xml_output .= "\t<item>\n"; 
        $xml_output .= "\t\t<nombre>" . $row['NombreCarrera'] . "</nombre>\n";
        $xml_output .= "\t\t<activo>" . $row['Activo'] . "</activo>\n";
		$xml_output .= "\t</item>\n"; 
    }
} 
$xml_output .= "</carrera>"; 

echo $xml_output; 
?>