<?php
header("Content-type: text/xml");  // Algo Nuevo y es la regla numero 1

$servername = "localhost";
$username = "root"; 
$password = "";
$dbname ="sistemadeinventario";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Conexión Fallida: " . $conn->connect_error);
} 

$sql = "SELECT IdPermiso, IdTipoUsuario, IdObjeto, Descripcion, Activo from permisos";

$result = $conn->query($sql);

$xml_output = "<?xml version=\"1.0\"?>\n";  
$xml_output .= "<permiso>\n"; 

if ($result->num_rows > 0) {

    while($row = $result->fetch_assoc()) {
        $xml_output .= "\t<item>\n"; 
        $xml_output .= "\t\t<idPermiso>" . $row['IdPermiso'] . "</idPermiso>\n";
        $xml_output .= "\t\t<idTipoUsuario>" . $row['IdTipoUsuario'] . "</idTipoUsuario>\n";
        $xml_output .= "\t\t<idObjeto>" . $row['IdObjeto'] . "</idObjeto>\n";
        $xml_output .= "\t\t<descripcion>" . $row['Descripcion'] . "</descripcion>\n";
        $xml_output .= "\t\t<activo>" . $row['Activo'] . "</activo>\n";
		$xml_output .= "\t</item>\n"; 
    }
} 
$xml_output .= "</permiso>"; 

echo $xml_output; 
?>