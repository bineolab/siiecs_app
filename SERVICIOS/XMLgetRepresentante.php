<?php
header("Content-type: text/xml");  // Algo Nuevo y es la regla numero 1

$servername = "localhost";
$username = "root"; 
$password = "";
$dbname ="sistemadeinventario";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Conexión Fallida: " . $conn->connect_error);
} 

$sql = "SELECT  Nombre,Idproveedor,Activo from representante where Identidad='".$_GET["identidad"]."'";

$result = $conn->query($sql);

$xml_output = "<?xml version=\"1.0\"?>\n";  
$xml_output .= "<representante>\n"; 

if ($result->num_rows > 0) {

    while($row = $result->fetch_assoc()) {
        $xml_output .= "\t<item>\n"; 
		
		$xml_output .= "\t\t<nombre>" . $row['Nombre'] . "</nombre>\n";
		$xml_output .= "\t\t<idproveedor>" . $row['Idproveedor'] . "</idproveedor>\n";
        $xml_output .= "\t\t<activo>" . $row['Activo'] . "</activo>\n";
		$xml_output .= "\t</item>\n"; 
    }
} 

$xml_output .= "</representante>"; 

echo $xml_output; 
?>