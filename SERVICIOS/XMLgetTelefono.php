<?php
header("Content-type: text/xml");  // Algo Nuevo y es la regla numero 1

$servername = "localhost";
$username = "root"; 
$password = "";
$dbname ="sistemadeinventario";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Conexión Fallida: " . $conn->connect_error);
} 

$sql = "SELECT A.IdTelefono, A.Numero, B.Descripcion, A.Activo from telefono A INNER JOIN tipotelefono B ON A.IdTipoTelefono= B.IdTipoTelefono where A.NumeroTH='".$_GET["numeroTH"]."'";

$result = $conn->query($sql);
$act="";
$xml_output = "<?xml version=\"1.0\"?>\n";  
$xml_output .= "<telefono>\n"; 

if ($result->num_rows > 0) {

    while($row = $result->fetch_assoc()) {
        $act = ( $row['Activo']==1 ) ? "Si" : "No"; 
        $xml_output .= "\t<item>\n"; 
        $xml_output .= "\t\t<idTelefono>" . $row['IdTelefono'] . "</idTelefono>\n";
        $xml_output .= "\t\t<numero>" . $row['Numero'] . "</numero>\n";
        $xml_output .= "\t\t<idTipoTelefono>" . $row['Descripcion'] . "</idTipoTelefono>\n";
        $xml_output .= "\t\t<activo>" .$act. "</activo>\n";
		$xml_output .= "\t</item>\n"; 
    }
} 

$xml_output .= "</telefono>"; 

echo $xml_output; 
?>