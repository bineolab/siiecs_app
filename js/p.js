function p(){
    const url = new URL('http://midominio.com/ruta/?param1=valor1&param2=valor2')

const searchParams = url.searchParams

const keys = [...searchParams.keys()]

const object1 = keys
  .reduce((obj, key) =>({...obj, [key]: searchParams.get(key) }), {})
  
const object2 = [...searchParams.entries()]
  .reduce((obj, [key, value]) => ({...obj, [key]: value }), {})

console.log(object1)
console.log(object2)

// [[key1, value1], ...]
console.log([...searchParams.entries()])
// [key1, key2, ...]
console.log([...searchParams.keys()])
// [value1, value2, ...]
console.log([...searchParams.values()])
}

p();