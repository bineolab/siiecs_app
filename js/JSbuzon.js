// Rq -> Requisicion
const buzon = new Vue({
    el: '#buzon',
    data : {
        hayDetalles: false, haySolicitudes : false, editado: false,
        solicitudes: [], solicitudActual: {}, detalleActual:{}
    },
    mounted(){
        this.getSolicitudes();
    },
    computed : {
        
    },
    methods: {
        getSolicitudes(){
            axios.get('../SERVICIOS/apiBuzon.php?action=getSolicitudes')
                .then( res => {
                    this.solicitudes = res.data.solicitudes;
                    //this.solicitudes.reverse();
                    console.log(res.data.solicitudes);
                    if(this.solicitudes.length == 0){
                        this.haySolicitudes = false;
                    }else{
                        this.haySolicitudes = true;
                    }                
                });
        },
        getDetallesSolicitud(idsolicitud){
            axios.get(`../SERVICIOS/apiBuzon.php?action=getDetalleSolicitud&idsolicitud=${idsolicitud}`)
                .then( res => { 
                    if(res.data.error == true){
                        mostrarMsj('ERROR: '+res.data.msg);
                    }else{
                        this.detalleActual = res.data.detalles;
                    }
                });
        },
        cambiarEstado(solicitudActual,idestado){
            axios(`../SERVICIOS/apiBuzon.php?action=cambiarEstado&IdEstado=${idestado}&IdSolicitud=${solicitudActual.IdSolicitud}`)
                .then( res => { 
                    if(res.data.error == true){
                        mostrarMsj('Error'+res.data.error);
                    }else{
                        //this.enviarCorreo(solicitudActual);
                        mostrarMsj(res.data.msg);
                    }
                });

        },
        enviarCorreo(solicitudActual){
            if(solicitudActual.IdEstado == 2){
                let mensaje = `Hola ${solicitudActual.Nombres}, su requisición fue: 
                ${solicitudActual.Nombre}.`
                axios.get(`../SERVICIOS/apiBuzon.php?action=enviarCorreo&
                    emailDocente=${solicitudActual.Correo}&mensaje=${mensaje}`).then( res => {
                    mostrarMsj(res.data.msg);
                    console.log(res);
                    $('#detalleRq').modal('hide');
                });
            }
        },
        mostrarRq(index){
            this.solicitudActual = this.solicitudes[index];
            console.log(this.solicitudActual);
            setTimeout(()=>{
                this.getDetallesSolicitud(this.solicitudActual.IdSolicitud);
                this.hayDetalles = true;
                console.log("Estos son los detalles: ",this.detalleActual);
            },1000);
            $('#detalleRq').modal('show');
            this.detalleActual = {};
        },
        calcularHoras(fechaPasada, ocupara){
            //console.log('La fecha es: un :'+typeof fechaPasada);
            var fechaRequisicion = fechaPasada.split("-");          
            var fechaOcupara= ocupara.split("-");
            var hoy = new Date(fechaOcupara[0], (fechaOcupara[1]-1), fechaOcupara[2]);
            var furturo = new Date(fechaRequisicion[0],(fechaRequisicion[1]-1),fechaRequisicion[2]);
            var diferencia = hoy.getTime() - furturo.getTime();
            var horasTranscurridas = Math.floor(diferencia / 1000 / 60 / 60);
            console.log(horasTranscurridas);
            return horasTranscurridas;
            
        },
        cambiarColor(rqActual,fechaSolicitud){
            let horasTranscurridas = this.calcularHoras(fechaSolicitud,rqActual);
            if( horasTranscurridas <= 36   ){
                return `<h2 title="Han transcurrido ${horasTranscurridas} horas..."><span class="badge badge-danger ">${rqActual}</span></h2>
                <small>Han transcurrido ${horasTranscurridas} horas... <small> `;
            }else if ( horasTranscurridas > 36 && horasTranscurridas <= 72 ){
                return `<h3 title="Han transcurrido ${horasTranscurridas} horas..."><span class="badge badge-warning text-white">${rqActual}</span>
                </h3><small>Han transcurrido ${horasTranscurridas} horas... <small>`;
            }else if ( horasTranscurridas > 72 ){
                return `<h2 title="Han transcurrido ${horasTranscurridas} horas..."><span class="badge badge-success text-white">${rqActual}</span></h2>
                <small>Han transcurrido más de 72 horas... <small>`;
            }
        },
        editarEntregado(index){
            if(!this.editado){
                this.editado = true;
            }else{
                this.editado = false;
            }
        },
        obtenerPeriodoRq(){
            let tiempo = new Date();
            if((tiempo.getMonth()+1) >= 0 && (tiempo.getMonth()+1) <= 3 ){
                return tiempo.getFullYear()+"1";
            }else if((tiempo.getMonth()+1) >= 3 && (tiempo.getMonth()+1) <= 6 ){
                return tiempo.getFullYear()+"2";
            }else if((tiempo.getMonth()+1) >= 6 && (tiempo.getMonth()+1) <= 9 ){
                return tiempo.getFullYear()+"3";
            }else if((tiempo.getMonth()+1) >= 9 && (tiempo.getMonth()+1) <= 12 ){
                return tiempo.getFullYear()+"4";
            }
        }

    }
});