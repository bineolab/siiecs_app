const formulario = new Vue({
    el: '#formulario',
    data: {
      idProveedor:0,numeroFactura:0, fechaCompra: "", articulos: [], DBArticulos:[],
      subTotal : 0, total : 0, idArticulo: 0, descripcion:"Seleccionar", articuloSeleccionado:{},
      cantidad: 0, precio: 0, estadoMaestro: false, idMaestro: 0, conIsv: true, indexActual: 0,
      // Actualizar articulo
      articuloActual: {}
    
    },
    methods: {
        insertMaestro (e){  
            axios.post('../SERVICIOS/api_maestroDetalle.php?action=createM', new FormData( e.target ))
                .then( res => {
                    switch(res.data.error){
                        case true:
                            toastMsj(res.data.msg);
                            break;
                        default:
                        console.log(res.data);
                            this.idMaestro = parseInt(res.data.ultimoID);
                            toastMsj(res.data.msg);
                            this.estadoMaestro = true;
                            mostrarMsj('El ID maestro es: '+this.idMaestro);
                            break;
                    }
                });
        },
        addArticulo () {
            if(this.cantidad > 0 && this.precio > 0 && this.articuloSeleccionado != null){
                this.articulos.push({
                    idArticulo: this.articuloSeleccionado.IdArticulo,
                    descripcion: this.articuloSeleccionado.Descripcion,
                    cantidad: this.cantidad, 
                    precio: this.precio,
                    isv:  this.conIsv == true ? true :  false,
                    subtotal: this.conIsv == true ? (this.cantidad * this.precio) * 1.15 : this.cantidad * this.precio
                });
               
                this.cantidad = 0;this.precio = 0; this.articuloSeleccionado = {};  
                
            }else{ toastMsj('Debe de llenar los campos para agregar un insumo a la factura'); }
        },
        
        deleteArticulo (index){
            this.articulos.splice(index, 1);
            this.subTotal = this.sumaSubTotal();
            this.total = this.sumaTotal();
        },
        
        getArticulos(){
            axios.get('../servicios/api_maestroDetalle.php?action=getArticulos')
            .then( res => {
                this.DBArticulos = res.data.articulos;
                console.log(res.data.articulos);
            });
        },
        showEditArticle(index){
            $('#editArticulo').modal('show');
            this.indexActual = index;
            this.articuloActual = this.articulos[index];
            //console.log('El articulo que desea modificar es: ',this.articuloActual);
        },
        keyupEditArticle(){
            this.articuloActual.subtotal = this.conIsv == true ? ( this.articuloActual.cantidad *  this.articuloActual.precio) * 1.15 : ( this.articuloActual.cantidad *  this.articuloActual.precio);
        },
        registrarDetalle(){
            if(this.articulos.length > 0){
                console.log("TAmaño del articulos: "+this.articulos.length);
                for(let i = 0; i < this.articulos.length; i++){
                    let url = '../SERVICIOS/api_maestroDetalle.php?action=createArticulo';
                    url = url + `&idMaestro=${this.idMaestro}
                    &cantidad=${parseInt(this.articulos[i].cantidad)}
                    &precio=${parseFloat(this.articulos[i].precio)}
                    &idArticulo=${parseInt(this.articulos[i].idArticulo)}
                    &subTotal=${parseFloat((this.articulos[i].subtotal).toFixed(2))}&total=${this.total}`;
                    axios.get(url).then( res => {
                        console.log("El resp de: ",res.data);
                        console.log("Datos de php: ",res.data.dataRecibida)
                        if(res.data.error == true){
                            toastMsj(res.data.msg);
                        }
                        toastMsj('Detalles Guardados Correctamente!');
                        mostrarMsj('Detalles Guardados Correctamente!\n'+res.data.msg);
                        setTimeout(()=>{
                            location.reload();
                        },3000);
                    });
                    console.log("Se ejecutó "+(i+1)+" veces");
                }                
            }else{mostrarMsj('Ingrese al menos 1 insumo a la factura');}
        }
    },
    mounted () {
        this.getArticulos();
    },
    computed: {
        sumaTotal(){
            this.total = 0;
            for(articulo of this.articulos){
                this.total += articulo.subtotal;
            }
            return (this.total).toFixed(2);
        },
        sumaSubTotal(){
            this.subTotal = 0;
            for(articulo of this.articulos){
                this.subTotal += articulo.subtotal;
            }
            return (this.subTotal).toFixed(2);   
        }
    },
  });
