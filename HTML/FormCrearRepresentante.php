<?php require_once "./Parciales/ValidacionDeSesion.php" ?>

<?php require_once "./Parciales/Encabezado.php" ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="../js/JSmensajeEnvio.js"></script>
<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once "./Parciales/MenuLateral.php" ?>
          <!-- End of Sidebar -->

          <!-- Content Wrapper -->
          <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

              <!-- Topbar -->
              <?php require_once "./Parciales/Top.php" ?>
              <!-- End of Topbar -->

              <!-- Begin Page Content -->
              <div class="container-fluid">

                <!-- Page Heading -->
         <!--<h1 class=" text-center">Articulo</h1>
          
          
          <h5 class="  text-gray-800 text-center d-lg-block">CENTRO UNIVERSITARIO TECNOLÓGICO</h5>-->
          
          
          
          <!-- Content Row -->
          <div class="row justify-content-center">
          <?php
					if($permisoActivo==1){
						echo '
            
            <div class="col-lg-12 col-xl-7  ">
            <div class="progress">
						<div class="progress-bar bg-info progress-bar-striped progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 50%">Paso 1- Completado</div>
						<div class="progress-bar  progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 25%">Paso 2</div>
           
						</div>
              <div class="card shadow mb-4 trans">
                <div class="card-header py-3">
                  <!--<img src="img/logo.png" class="img-responsive" width="50%">-->
                  <h3 class="m-0 font-weight-bold text-primary">Crear Representante</h3>
                  
                </div>
                <div class="card-body">
                  <form action="PHPCrearRepresentante.php" method="POST">
				   <input type="hidden"  id="IdSesion" name="IdSesion"></input>
                    <div class="row">
                      <div class="form-group col-sm-12 col-md-6">
                   
					   
					<div class="form-group">
                <label for="identidad">Identidad</label>
               <input type="text" class="form-control" id="identidad" name="identidad" placeholder="identidad">
				
			  
            </div>
				<div class="form-group">
                <label for="nombre">Nombre</label>
               <input type="text" class="form-control" id="nombre" name="nombre" placeholder="nombre">
				
			  
            </div>
			     <div class="form-group col-sm-12 col-md-6">
                <label for="idproveedor">Id Proveedor</label>
                <select class="form-control" id="idproveedor" name="idproveedor">';
                 
                  $servername = "localhost";
                  $username = "root";
                  $password = "";
                  $dbname = "sistemadeinventario";

                    // Create connection
                  $conn = new mysqli($servername, $username, $password, $dbname);
                    // Check connection
                  if ($conn->connect_error) {
                   die("Connection failed: " . $conn->connect_error);
                 } 

                 $sql = "SELECT IdProveedor, Nombre from proveedores";
                 $result= $conn->query($sql);

                 if ($result->num_rows>0) {
                   while($row=$result->fetch_assoc()){
                    echo "<option value='".$row["IdProveedor"]."'>".$row["Nombre"]."</option>";
                  }
                } else {
                  echo "<option> --Tabla vacia--</option>";
                }

                $conn->close();
                echo
                
              '</select>
            </div>
			
                       
                  <div class="form-group">
                  <label for="activo">¿Está activo?:</label>
                 <select class="form-control" id="activo" name="activo">
				 <option value="1">SI</option>
				 <option value="0">NO</option>
				 </select>
                 
              </div>
              
            
            
            
            <button type="submit" class="btn btn-primary ">Guardar</button>
            
          </form>
        </div>
      </div>
      
      
      

    </div>';
        }else{
    echo '
    <div class="col-lg-12 col-xl-7 ">
      <div class="card shadow mb-4 trans">
        <div class="card-header py-3">
          <!--<img src="img/logo.png" class="img-responsive" width="50%">-->
          <h3 class="m-0 font-weight-bold text-primary">No tiene acceso</h3>

        </div>
      </div>
    </div>
    ';
}
?>

    <!-- Donut Chart -->
    
  </div>

  <?php require_once "./Parciales/Scripts.php"?>
  <script type="text/javascript" src="../js/JSenviarIdSesion.js"></script>
</body>

</html>