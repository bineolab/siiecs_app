<?php require_once "./Parciales/ValidacionDeSesion.php" ?>
<?php require_once "./Parciales/Encabezado.php" ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="../js/JSmensajeEnvio.js"></script>
<body id="page-top">

   <!-- Page Wrapper -->
   <div id="wrapper">

      <!-- Sidebar -->
      <?php require_once "./Parciales/MenuLateral.php" ?>
      <!-- End of Sidebar -->

      <!-- Content Wrapper -->
      <div id="content-wrapper" class="d-flex flex-column">

       <!-- Main Content -->
       <div id="content">

          <!-- Topbar -->
          <?php require_once "./Parciales/Top.php" ?>
          <!-- End of Topbar -->

          <!-- Begin Page Content -->
          <div class="container-fluid">

             <!-- Page Heading -->
         <!--<h1 class=" text-center">Registro de Usuario</h1>
          
          
          <h5 class="  text-gray-800 text-center d-lg-block">CENTRO UNIVERSITARIO TECNOLÓGICO</h5>-->



          <!-- Content Row -->
          <div class="row d-flex justify-content-center">
          <?php
			if($permisoActivo==1){
			echo '

             <div class="col-lg-12 col-xl-5 ">
                <div class="card shadow mb-4 trans">
                   <div class="card-header py-3">
                      <!--<img src="img/logo.png" class="img-responsive" width="50%">-->
                      <h3 class="m-0 font-weight-bold text-primary">Actualizar artículo</h3>

                  </div>
                  <div class="card-body">

                      <div class=" d-sm-inline-block form-inline  navbar-search">
                         <div class="input-group">
                            <input type="text" class="form-control bg-light border-0 small" placeholder="Ingrese ID del artículo" aria-label="idArticulo" aria-describedby="basic-addon2" id="idArticulo" name="idArticulo">
                            <div class="input-group-append">
                               <button class="btn btn-primary" type="button" onclick="BuscarArticulo();">
                                  <i class="fas fa-search fa-sm"></i>
                              </button>
                          </div>
                      </div>
                  </div>
                  <br>
                  <br>
                  <form action="PHPActualizarArticulo.php" method="POST">
                  
                  <input type="hidden"  id="IdSesion" name="IdSesion"></input>
                     <div class="form-group">
                      <input type="hidden" id="idArticulo2" name="idArticulo2">

                      <div class="row">
                       <div class="form-group col-sm-12 col-md-8">
                          <label for="descripcion">Nombre</label>
                          <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Nombre del artículo">
                      </div>
                      <div class="form-group col-sm-12 col-md-8">
                      <label for="detalle">Detalle</label>
                      <input type="text" class="form-control" id="detalle" name="detalle" placeholder="Detalle del artículo">
                  </div>

                      <div class="form-group col-sm-12 col-md-4">
                          <label for="nombrePractica">¿Está activo?</label>
                          <select class="form-control" name="activo" id="activo">
                              <option value="">--</option>
                              <option value="1">SI</option>
                              <option value="0">NO</option>
                          </select>
                      </div>
                      <div class="form-group col-sm-12 col-md-4">
                          <label for="idTipoArticulo">Tipo artículo</label>
                          <select id="idTipoArticulo" class="form-control" name="idTipoArticulo">

                          <option value="">--</option>
                              ';
                              $servername = "localhost";
                              $username = "root";
                              $password = "";
                              $dbname = "sistemadeinventario";

                    // Create connection
                              $conn = new mysqli($servername, $username, $password, $dbname);
                    // Check connection
                              if ($conn->connect_error) {
                               die("Connection failed: " . $conn->connect_error);
                           }

                           $sql = "SELECT IdTipoArticulo, Descripcion from tipoarticulo";
                           $result= $conn->query($sql);

                           if ($result->num_rows>0) {
                               while($row=$result->fetch_assoc()){
                                 echo "<option value='".$row["IdTipoArticulo"]."'>".$row["Descripcion"]."</option>";
                             }
                         } else {
                            echo "<option> --Tabla vacia--</option>";
                        }

                        $conn->close();

                        echo '
                    </select>
               
                
                </div>
            </div>

            <button type="submit" class="btn btn-primary ">Actualizar Artículo</button>
            </div>
        </form>

        </div>
        </div>
        </div>



<div class="col-sm-12 col-md-5">
   <div class="card shadow mb-4 trans">
    <div class="card-header py-3">
        <!--<img src="img/logo.png" class="img-responsive" width="50%">-->
        <h3 class="m-0 font-weight-bold text-primary">Artículos existentes</h3>
    </div>
    <div class="card-body">
       <div class="table-responsive">
        <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>Id del artículo</th>
                    <th>Descripción</th>
                    <th>Activo</th>
                    
                </tr>
            </thead>
            <tbody>
                ';
                $servername = "localhost";
                $username = "root";
                $password = "";
                $dbname = "sistemadeinventario";

                                                        // Create connection
                $conn = new mysqli($servername, $username, $password, $dbname);
                                                        // Check connection
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }

                $sql = "SELECT IdArticulo, Descripcion, Activo from articulo";
                $result= $conn->query($sql);
                $act="";
                if ($result->num_rows>0) {
                    while($row=$result->fetch_assoc()){
                        $act = ( $row['Activo']==1 ) ? "Si" : "No"; 
                        print "<tr>
                        <td>".$row['IdArticulo']."</td>".
                        "<td>".$row['Descripcion']."</td>".
                        "<td>".$act."</td>".
                        "</tr>";
                    }
                } else {
                    echo "<option> --Tabla vacia--</option>";
                }

                $conn->close();

                echo '
            </tbody>
        </table>                         
    </div>
</div>
</div>
</div>
</div>';
}else{
    echo '
    <div class="col-lg-12 col-xl-7 ">
      <div class="card shadow mb-4 trans">
        <div class="card-header py-3">
          <!--<img src="img/logo.png" class="img-responsive" width="50%">-->
          <h3 class="m-0 font-weight-bold text-primary">No tiene acceso</h3>

        </div>
      </div>
    </div>
    ';
}
?>


<!-- Donut Chart -->

</div>

<?php require_once "./Parciales/Scripts.php"?>
<script type="text/javascript" src="../js/JSActualizarArticulo.js"></script>
<script type="text/javascript" src="../js/JSenviarIdSesion.js"></script>

</body>

</html>