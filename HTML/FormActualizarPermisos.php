<?php require_once "./Parciales/ValidacionDeSesion.php" ?>
<?php require_once "./Parciales/Encabezado.php" ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="../js/JSmensajeEnvio.js"></script>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once "./Parciales/MenuLateral.php" ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php require_once "./Parciales/Top.php" ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
         <!--<h1 class=" text-center">Registro de Usuario</h1>


          <h5 class="  text-gray-800 text-center d-lg-block">CENTRO UNIVERSITARIO TECNOLÓGICO</h5>-->



          <!-- Content Row -->
          <div class="row justify-content-center">
          <?php
			if($permisoActivo==1){
			echo '

            <div class="col-lg-12 col-xl-5 ">
              <div class="card shadow mb-4 trans">
                <div class="card-header py-3">
                  <!--<img src="img/logo.png" class="img-responsive" width="50%">-->
                  <h3 class="m-0 font-weight-bold text-primary">Actualizar permiso</h3>

                </div>
                <div class="card-body">
                  <div class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                    <div class="input-group">
                      <input type="text" class="form-control bg-light border-0 small" placeholder="Ingrese ID del permiso" aria-label="idPermiso" aria-describedby="basic-addon2" id="idPermiso" name="idPermiso">
                      <div class="input-group-append">
                        <button class="btn btn-primary" type="button" onclick="BuscarPermiso();">
                          <i class="fas fa-search fa-sm"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                  <br>
                  <br>
                  <form action="PHPActualizarPermisos.php" method="POST">
                  <input type="hidden"  id="IdSesion" name="IdSesion"></input>
                    <div class="row">
                     <input type="hidden"  id="idPermiso2" name="idPermiso2"></input>

                     <div class="form-group col-sm-12 col-md-6">
                      <label for="idTipoUsuario">Categoria de usuario *</label>
                      <select class="form-control" id="idTipoUsuario" name="idTipoUsuario" required>
                       <option value="">--</option>
                     ';
                       $servername = "localhost";
                       $username = "root";
                       $password = "";
                       $dbname = "sistemadeinventario";

                    // Create connection
                       $conn = new mysqli($servername, $username, $password, $dbname);
                    // Check connection
                       if ($conn->connect_error) {
                         die("Connection failed: " . $conn->connect_error);
                       }

                       $sql = "SELECT idTipoUsuario, Descripcion from tipousuario";
                       $result= $conn->query($sql);

                       if ($result->num_rows>0) {
                         while($row=$result->fetch_assoc()){
                          echo "<option value='".$row["idTipoUsuario"]."'>".$row["Descripcion"]."</option>";
                        }
                      } else {
                        echo "<option> --Tabla vacia--</option>";
                      }

                      $conn->close();

                   echo'
                    </select>
                  </div>
                  

                  <div class="form-group col-sm-12 col-md-6">
                      <label for="idTipoUsuario">Pantalla a permitir *</label>
                      <select class="form-control" id="idObjeto" name="idObjeto" required>
                       <option value="">--</option>
                     ';
                       $servername = "localhost";
                       $username = "root";
                       $password = "";
                       $dbname = "sistemadeinventario";

                    // Create connection
                       $conn = new mysqli($servername, $username, $password, $dbname);
                    // Check connection
                       if ($conn->connect_error) {
                         die("Connection failed: " . $conn->connect_error);
                       }

                       $sql = "SELECT IdObjeto, Nombre from objetosdelsistema";
                       $result= $conn->query($sql);

                       if ($result->num_rows>0) {
                         while($row=$result->fetch_assoc()){
                          echo "<option value='".$row["IdObjeto"]."'>".$row["Nombre"]."</option>";
                        }
                      } else {
                        echo "<option> --Tabla vacia--</option>";
                      }

                      $conn->close();

                      echo'
                    </select>
                  </div>


               <div class="form-group col-sm-12 col-md-6">
                <label for="descripcion">Comentario</label>
                <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Comentario">
              </div>


              <div class="form-group col-sm-12 col-md-6">
                <label for="activo">¿Está activo? *</label>       
                <select class="form-control" id="activo" name="activo" required>
                  <option value="">--</option>
                  <option value="1">SI</option>
                  <option value="0">NO</option>
                </select>
              </div>
              </div>
              <button type="submit" class="btn btn-primary ">Actualizar permiso</button>

            </form>
            <br><br>
            <!-- Tabla-->
            
                 <!-- Final Tabla-->
          </div>
        </div>

        </div>

        <div class="col-lg-12 col-xl-6">
						 <div class="card shadow mb-4 trans">
								<div class="card-header py-3">
				 					<!--<img src="img/logo.png" class="img-responsive" width="50%">-->
				 					<h3 class="m-0 font-weight-bold text-primary">Permisos de los tipos de usuarios</h3>
				 				</div>
								 <div class="card-body">
                 <div class="table-responsive" id="tabla">
                <table class="table table-striped" id="tabla" width="100%" cellspacing="0">
                              <thead>
                                <tr>
                                  <th scope="col">ID del permiso</th>
                                  <th scope="col">ID tipo usuario</th>
                                  <th scope="col">IdObjeto</th>
                                  <th scope="col">Comentario</th>
                                  <th scope="col">Activo</th>
                                </tr>
                              </thead>
                              <tbody id="tablaBody">

                                    
                             </tbody>
                      </table>
                  </div>
								 
								 </div>
                 </div>';
                }else{
                    echo '
                    <div class="col-lg-12 col-xl-7 ">
                      <div class="card shadow mb-4 trans">
                        <div class="card-header py-3">
                          <!--<img src="img/logo.png" class="img-responsive" width="50%">-->
                          <h3 class="m-0 font-weight-bold text-primary">No tiene acceso</h3>
                
                        </div>
                      </div>
                    </div>
                    ';
                }
                ?>
                

      <!-- Donut Chart -->

    </div>

    <?php require_once "./Parciales/Scripts.php"?>
    <script type="text/javascript" src="../js/JSActualizarPermisos.js"></script>
    <script type="text/javascript" src="../js/JSBusquedaTablaPermisos.js"></script>
    <script type="text/javascript" src="../js/JSenviarIdSesion.js"></script>
  </body>

  </html>
