<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

<!-- Sidebar - Brand -->
<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
  <div class="sidebar-brand-icon rotate-n-15">
    <i class="fas fa-laugh-wink"></i>
  </div>
  <div class="sidebar-brand-text mx-3">SIIECS</div>
</a>

<!-- Divider -->
<hr class="sidebar-divider my-0">

<!-- Nav Item - Dashboard -->
<li class="nav-item active">
  <a class="nav-link" href="#">
    <i class="fas fa-fw fa-tachometer-alt"></i>
    <span>Inicio</span></a>
</li>

<!-- Divider -->
<hr class="sidebar-divider">

<!-- Heading -->
<div class="sidebar-heading">
  Interface
</div>

<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
    <i class="fas fa-fw fa-cog"></i>
    <span>Módulo de seguridad</span>
  </a>
  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <h6 class="collapse-header">Usuarios:</h6>
            <a class="collapse-item" href="FormCrearUsuarios.php">Crear usuarios</a>
            <a class="collapse-item" href="FormActualizarUsuarios.php">Actualizar usuarios</a>
            <a class="collapse-item" href="FormCrearTelefono.php">Crear teléfonos</a>
            <a class="collapse-item" href="FormActualizarTelefono.php">Actualizar teléfonos</a>
            <a class="collapse-item" href="FormCrearTipoTelefono.php">Crear tipo de teléfonos</a>
            <a class="collapse-item" href="FormActualizarTipoTelefono.php">Actualizar tipo de teléfonos</a>
            <h6 class="collapse-header">Roles:</h6>
            <a class="collapse-item" href="FormCrearTipoUsuario.php">Crear tipo de usuarios</a>
            <a class="collapse-item" href="FormActualizarTipoUsuario.php">Actualizar tipo de usuarios</a>
            <a class="collapse-item" href="FormCrearPermisos.php">Crear permisos</a>
            <a class="collapse-item" href="FormActualizarPermisos.php">Actualizar permisos</a>
    </div>
  </div>
</li>

<!-- Nav Item - Utilities Collapse Menu -->
<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
    <i class="fas fa-fw fa-wrench"></i>
    <span>Módulo de inventario</span>
  </a>
  <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
    <h6 class="collapse-header">Insumos, equipo y</h6>
            <h6 class="collapse-header">simuladores:</h6>
            <a class="collapse-item" href="FormCrearArticulo.php">Crear artículo</a>
            <a class="collapse-item" href="FormActualizarArticulo.php">Actualizar artículo</a>
            <a class="collapse-item" href="FormCrearTipoArticulo.php">Crear tipo de artículo</a>
            <a class="collapse-item" href="FormActualizarTipoArticulo.php">Actualizar tipo de artículo</a>
            <h6 class="collapse-header">Proveedores:</h6>
            <a class="collapse-item" href="FormCrearProveedor.php">Crear proveedores</a>
            <a class="collapse-item" href="FormActualizarProveedor.php">Actualizar proveedores</a>
            <a class="collapse-item" href="FormCrearRepresentante.php">Crear representante</a>
            <a class="collapse-item" href="FormActualizarRepresentante.php">Actualizar representante</a>

    </div>
  </div>
</li>

<!-- Divider -->
<hr class="sidebar-divider">

<!-- Heading -->
<div class="sidebar-heading">
  Addons
</div>

<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
    <i class="fas fa-fw fa-folder"></i>
    <span>Cuenta</span>
  </a>
  <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <h6 class="collapse-header">Login Screens:</h6>
      <a class="collapse-item" href="FormLogin.php">Login</a>
      <a class="collapse-item" href="#">Register</a>
      <a class="collapse-item" href="#">Forgot Password</a>
      <div class="collapse-divider"></div>
      <h6 class="collapse-header">Other Pages:</h6>
      <a class="collapse-item" href="#">404 Page</a>
      <a class="collapse-item" href="#">Blank Page</a>
    </div>
  </div>
</li>

<!-- Nav Item - Charts -->
<li class="nav-item">
  <a class="nav-link" href="#">
    <i class="fas fa-fw fa-chart-area"></i>
    <span>Requisiciones</span></a>
</li>

<!-- Nav Item - Tables -->
<li class="nav-item">
  <a class="nav-link" href="#">
    <i class="fas fa-fw fa-table"></i>
    <span>Reportes</span></a>
</li>

<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">

<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline">
  <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>

</ul>
<!-- End of Sidebar -->