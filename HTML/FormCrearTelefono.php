<?php require_once "./Parciales/ValidacionDeSesion.php" ?>

<?php require_once "./Parciales/Encabezado.php" ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="../js/JSmensajeEnvio.js"></script>

<body id="page-top">

	<!-- Page Wrapper -->
	<div id="wrapper">

		<!-- Sidebar -->
		<?php require_once "./Parciales/MenuLateral.php" ?>
		<!-- End of Sidebar -->

		<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">

			<!-- Main Content -->
			<div id="content">

				<!-- Topbar -->
				<?php require_once "./Parciales/Top.php" ?>
				<!-- End of Topbar -->


				<!-- Begin Page Content -->
				<div class="row justify-content-center">

					<?php
					if($permisoActivo==1){
						echo '
						<div class="col-lg-12 col-xl-7  ">
						<div class="progress">
						<div class="progress-bar bg-info progress-bar-striped progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 50%">Paso 1- Completado</div>
						<div class="progress-bar  progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 50%">Paso 2</div>
							
						</div>
						<div class="card shadow mb-4 trans">
							<div class="card-header py-3">
								<!--<img src="img/logo.png" class="img-responsive" width="50%">-->
								<h3 class="m-0 font-weight-bold text-primary">Registrar teléfono</h3>

							</div>
							<div class="card-body">
								<form action="PHPCrearTelefono.php" method="POST">
								<input type="hidden"  id="IdSesion" name="IdSesion"></input>
									<div class="form-group">
										<label for="horaClase">Talento humano *</label>
										<input type="text" name="numeroTH" class="form-control" id="numeroTH" placeholder="Talento humano" required>
									</div>
									<div class="row">
										<div class="form-group col-sm-12 col-md-4">
											<label for="asignatura">Número *</label>
											<input type="text" name="numero" class="form-control" id="numero" placeholder=" 0000-0000 ext 00" required>
										</div>
										<div class="form-group col-sm-12 col-md-4">
											<label for="nombrePractica">Tipo teléfono *</label>
											<select id="cb_tipoTelefonos" class="form-control" name="idTipoTelefono" required>

											</select>
										</div>

									</div>
									<div class="form-group">
										<button type="submit" class="btn btn-primary ">Registrar</button>

									</div>
								</form>
							</div>
						</div>

					</div>
						';
					}else{
						echo '
						<div class="col-lg-12 col-xl-7 ">
							<div class="card shadow mb-4 trans">
								<div class="card-header py-3">
									<!--<img src="img/logo.png" class="img-responsive" width="50%">-->
									<h3 class="m-0 font-weight-bold text-primary">No tiene acceso</h3>

								</div>
							</div>
						</div>
						';
					}
					?>

					

				</div>
				<!-- /.container-fluid -->

			</div>
			<!-- End of Main Content -->

			<!-- Footer -->
			<footer class="sticky-footer bg-white">
				<div class="container my-auto">
					<div class="copyright text-center my-auto">
						<span>Copyright &copy; Your Website 2019</span>
					</div>
				</div>
			</footer>
			<!-- End of Footer -->

		</div>
		<!-- End of Content Wrapper -->

	</div>
	<!-- End of Page Wrapper -->

	<!-- Scroll to Top Button-->
	<a class="scroll-to-top rounded" href="#page-top">
		<i class="fas fa-angle-up"></i>
	</a>



	

	<?php require_once "./Parciales/Scripts.php"?>
	<script type="text/javascript" src="../js/JSconsultaFormCrearTelefono.js"></script>
	<script type="text/javascript" src="../js/JSenviarIdSesion.js"></script>
	<script type="text/javascript" src="../js/JSenviarNumeroTH.js"></script>
</body>

</html>
