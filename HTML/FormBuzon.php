<?php  require_once "../config.php"; ?>
<?php require_once "./Parciales/Encabezado.php"; ?>

<style>

.loader {
  color: #5671ef;
  font-size: 20px;
  margin: 100px auto;
  width: 1em;
  height: 1em;
  border-radius: 50%;
  position: relative;
  text-indent: -9999em;
  -webkit-animation: load4 1.3s infinite linear;
  animation: load4 1.3s infinite linear;
  -webkit-transform: translateZ(0);
  -ms-transform: translateZ(0);
  transform: translateZ(0);
}
@-webkit-keyframes load4 {
  0%,
  100% {
    box-shadow: 0 -3em 0 0.2em, 2em -2em 0 0em, 3em 0 0 -1em, 2em 2em 0 -1em, 0 3em 0 -1em, -2em 2em 0 -1em, -3em 0 0 -1em, -2em -2em 0 0;
  }
  12.5% {
    box-shadow: 0 -3em 0 0, 2em -2em 0 0.2em, 3em 0 0 0, 2em 2em 0 -1em, 0 3em 0 -1em, -2em 2em 0 -1em, -3em 0 0 -1em, -2em -2em 0 -1em;
  }
  25% {
    box-shadow: 0 -3em 0 -0.5em, 2em -2em 0 0, 3em 0 0 0.2em, 2em 2em 0 0, 0 3em 0 -1em, -2em 2em 0 -1em, -3em 0 0 -1em, -2em -2em 0 -1em;
  }
  37.5% {
    box-shadow: 0 -3em 0 -1em, 2em -2em 0 -1em, 3em 0em 0 0, 2em 2em 0 0.2em, 0 3em 0 0em, -2em 2em 0 -1em, -3em 0em 0 -1em, -2em -2em 0 -1em;
  }
  50% {
    box-shadow: 0 -3em 0 -1em, 2em -2em 0 -1em, 3em 0 0 -1em, 2em 2em 0 0em, 0 3em 0 0.2em, -2em 2em 0 0, -3em 0em 0 -1em, -2em -2em 0 -1em;
  }
  62.5% {
    box-shadow: 0 -3em 0 -1em, 2em -2em 0 -1em, 3em 0 0 -1em, 2em 2em 0 -1em, 0 3em 0 0, -2em 2em 0 0.2em, -3em 0 0 0, -2em -2em 0 -1em;
  }
  75% {
    box-shadow: 0em -3em 0 -1em, 2em -2em 0 -1em, 3em 0em 0 -1em, 2em 2em 0 -1em, 0 3em 0 -1em, -2em 2em 0 0, -3em 0em 0 0.2em, -2em -2em 0 0;
  }
  87.5% {
    box-shadow: 0em -3em 0 0, 2em -2em 0 -1em, 3em 0 0 -1em, 2em 2em 0 -1em, 0 3em 0 -1em, -2em 2em 0 0, -3em 0em 0 0, -2em -2em 0 0.2em;
  }
}
@keyframes load4 {
  0%,
  100% {
    box-shadow: 0 -3em 0 0.2em, 2em -2em 0 0em, 3em 0 0 -1em, 2em 2em 0 -1em, 0 3em 0 -1em, -2em 2em 0 -1em, -3em 0 0 -1em, -2em -2em 0 0;
  }
  12.5% {
    box-shadow: 0 -3em 0 0, 2em -2em 0 0.2em, 3em 0 0 0, 2em 2em 0 -1em, 0 3em 0 -1em, -2em 2em 0 -1em, -3em 0 0 -1em, -2em -2em 0 -1em;
  }
  25% {
    box-shadow: 0 -3em 0 -0.5em, 2em -2em 0 0, 3em 0 0 0.2em, 2em 2em 0 0, 0 3em 0 -1em, -2em 2em 0 -1em, -3em 0 0 -1em, -2em -2em 0 -1em;
  }
  37.5% {
    box-shadow: 0 -3em 0 -1em, 2em -2em 0 -1em, 3em 0em 0 0, 2em 2em 0 0.2em, 0 3em 0 0em, -2em 2em 0 -1em, -3em 0em 0 -1em, -2em -2em 0 -1em;
  }
  50% {
    box-shadow: 0 -3em 0 -1em, 2em -2em 0 -1em, 3em 0 0 -1em, 2em 2em 0 0em, 0 3em 0 0.2em, -2em 2em 0 0, -3em 0em 0 -1em, -2em -2em 0 -1em;
  }
  62.5% {
    box-shadow: 0 -3em 0 -1em, 2em -2em 0 -1em, 3em 0 0 -1em, 2em 2em 0 -1em, 0 3em 0 0, -2em 2em 0 0.2em, -3em 0 0 0, -2em -2em 0 -1em;
  }
  75% {
    box-shadow: 0em -3em 0 -1em, 2em -2em 0 -1em, 3em 0em 0 -1em, 2em 2em 0 -1em, 0 3em 0 -1em, -2em 2em 0 0, -3em 0em 0 0.2em, -2em -2em 0 0;
  }
  87.5% {
    box-shadow: 0em -3em 0 0, 2em -2em 0 -1em, 3em 0 0 -1em, 2em 2em 0 -1em, 0 3em 0 -1em, -2em 2em 0 0, -3em 0em 0 0, -2em -2em 0 0.2em;
  }
}

.fontSolicitud{
  font-size: 2em;
}

</style>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    
    <?php require_once "./Parciales/MenuLateral.php" ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php require_once "./Parciales/Top.php" ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <main id="buzon">
            <div class="container-fluid">

            <!-- Page Heading -->
            
                    <!-- Alertas -->
                <!--<div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Requisición Actualizada!</strong> La requisición se actualizó en la base de datos.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>-->
            
            <!-- Content Row -->

            <!-- Template 1 -->
              
            <!-- Template 1 end -->


            <div class="row justify-content-center">
                    
                <div class="col-lg-12  col-md-12 col-xl-12 ">
                    <div class="card shadow trans mb-4">
                        <div class="card-header py-3">
                        <h3 class="m-0 font-weight-bold text-primary"> <i class="far fa-envelope-open"></i> Bandeja de requisiciones</h3>
                        </div>
                        <div class="card-body">
                            <div class="list-group">
                            <div v-if="!haySolicitudes" class="loader">Loading...</div>
                            <!-- data-target="#detalleRq" -->
                              <a v-if="haySolicitudes" v-for="(item,index) in solicitudes" 
                                class="list-group-item list-group-item-action flex-column align-items-start"
                                style="cursor:pointer" 
                                  data-toggle="modal"  @click="mostrarRq(index)">
                                  <div class="row">
                                        <div class="col-sm-12 col-md-1">
                                          <h1><i class=" fontSolicitud text-info fas fa-file-contract"></i></h1>
                                        </div>
                                        <div class="col-sm-12 col-md-8">
                                          <h3 class="mb-1 text-info">{{obtenerPeriodoRq()}}R{{ item.IdSolicitud }}-{{ item.NombrePractica ? item.NombrePractica : item.NombreClase   }}</h3>
                                          <p class="">Docente:  {{ item.Nombres }} {{ item.Apellidos }}.<br>
                                          Solicitud realizada {{ item.FechaSolicitud }}.
                                          <br> Estado de la solicitud: <strong>{{ item.Nombre }}</strong><br>

                                          </p>
                                        </div>
                                        <div class="col-sm-12 col-md-3 text-right" v-html="cambiarColor(item.Fecha,item.FechaSolicitud) ? cambiarColor(item.Fecha,item.FechaSolicitud) : 'Algo anda mal'">
                                          
                                        </div>
                                    </div>
                                    
                                    
                                </a>
                                
                            </div>
                
                        </div>
                    </div>
            

                </div>

                <!-- Modal Requisicion	-->
                
                <div class="modal fade" id="detalleRq" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                    
                        <!-- Modal Header -->
                        <div class="modal-header">
                        <h4 class="modal-title">Primeros Auxilios - 15/02/2019</h4>
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="card p-2">
                            <div class="card-header text-center text-primary font-weight-bold">
                                Información general
                            </div>
                            <div class="row">
                                    <div class="col-sm-12 col-md-3 form-group">
                                        <label for="asignatura">Nombre de la Asignatura</label>
                                        <select type="text" class="form-control" id="asignatura" disabled name="asignatura" placeholder="Asignatura">
                                            <option v-text="solicitudActual.NombreClase">Primeros Auxilios</option>
                                        </select>
                                    </div>
                                    <div class=" col-sm-12 col-md-3">
                                            <label for="seccion">Sección</label>
                                            <input type="text" disabled class="form-control"  v-model="solicitudActual.Seccion" placeholder="Sección" name="seccion">
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                                <label for="horaClase">Hora de la clase</label>
                                                <input type="time" disabled class="form-control" v-model="solicitudActual.HoraClase" id="horaClase" placeholder="Hora de la clase" name="horaClase">
                                        </div>
                                        <div class=" col-sm-12 col-md-3">
                                                <label for="fechaPractica">Fecha de la práctica		
                                                    </label>
                                                <input type="date" disabled class="form-control"  v-model="solicitudActual.Fecha" id="fechaPractica" placeholder="Fecha a realizarse la práctica" name="fechaPractica">
                                        </div>
                            </div>
                            <div class="form-group">
                                    <label for="nombrePractica">Nombre de la Práctica</label>
                                    <input type="text" disabled class="form-control" v-model="solicitudActual.NombrePractica" id="nombrePractica" name="nombrePractica" placeholder="Nombre de la Práctica">
                                </div>
                    
                               <div class="row ">
                                        <div class="col-sm-12 col-md-3">
                                                <label for="horaClase">Hora Inicio de Práctica</label>
                                                <input type="time" disabled class="form-control" id="horaInicio"  v-model="solicitudActual.HoraInicio" placeholder="Hora de la clase" name="horaClase">
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                                <label for="horaClase">Hora Final de Práctica</label>
                                                <input type="time" disabled class="form-control" id="horaFinal" v-model="solicitudActual.HoraFin" placeholder="Hora de la clase" name="horaClase">
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                          <label for="docente">Numero Talento Humano</label>
                                          <input type="text" disabled class="form-control" id="docente" v-model="solicitudActual.NumeroTH" name="docente" placeholder="12330">
                                      </div>
                                      <div class="col-sm-12 col-md-3">
                                          <label for="docente">Nombre del Docente</label>
                                          <input type="text" disabled class="form-control" id="docente" v-model="solicitudActual.Nombres"  readonly name="docente" placeholder="Docente">
                                      </div>
                                </div>
                            </div>
                            <br>
                        <div  class="card">
                            <div class="card-header text-center text-primary font-weight-bold">
                                Solicitud de insumos
                            </div>
                            <div v-if="!hayDetalles" class="loader">Loading...</div>
                            <ul v-if="hayDetalles" class="list-group list-group-flush">
                                <li class="list-group-item">
                            <div class="table-responsive">
                            <table class="table  table-striped responsive-table" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                <th>Código</th>
                                <th>Nombre</th>
                                <th>Tipo inventario</th>
                                <th>Cantidad</th>
                                <th>Disponibles</th>
                                <th>Entregado</th>
                                <th>Devuelto</th>
                                <th>Observaciones</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                <tr v-for="(item,index) in detalleActual">
                                <td>{{ item.IdArticulo }}</td>
                                <td>{{ item.NombreArticulo }}</td>
                                <td>{{ item.NombreTipoArticulo }}</td>
                                <td>{{ item.CantidadSolicitada }}</td>
                                <td class="text-success font-weight-bold">-</td>
                                <td> 
                                    <span v-if="!editado"> {{ item.CantidadDevolucion }} </span>
                                    <span v-if="editado">
                                      <input type="text" class="form-control col-md-2" v-model="item.CantidadDevolucion">
                                    </span>
                                    
                                    <span v-if="!(solicitudActual.IdEstado == 4)" >|
                                    <button type="button"
                                        class="btn btn-primary btn-circle btn-sm d-inline-block"
                                        title="Editar articulo"
                                        @click="editarEntregado(index)"
                                       > 
                                        <i class="fas fa-pencil-alt"></i>
                                    </button>
                                    </span>
                                    
                                </td>
                                <td>-</td>
                                <td>-</td>
                                </tr>
                            
                            </tbody>
                            </table>
                              <!--<pre> {{detalleActual}} </pre>-->
                                </div></li>
                            </ul>
                            </div>
                        </div>
                        
                        <!-- Modal footer -->
                        <div class="modal-footer">
                                
                                  <button v-if="!(solicitudActual.IdEstado == 2) || solicitudActual.IdEstado == 5"
                                  type="submit" class="btn btn-success " @click="cambiarEstado(solicitudActual,2)">Aceptar Requisición</button>
                                  <button  v-if="!(solicitudActual.IdEstado == 2)" 
                                  type="submit" class="btn btn-warning " data-toggle="modal" data-target="#rechazo" data-dismiss="modal" >Rechazar</button>
                                  <button v-if="!(solicitudActual.IdEstado == 5)"  
                                   type="submit" class="btn btn-primary ">Actualizar</button>
                                
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        
                        </div>
                        
                    </div>
                    </div>
                </div><!--modal requisicion-->
                
                    <!-- Modal rechazo -->
                    <div class="modal fade" id="rechazo">
                            <div class="modal-dialog modal-md">
                        <div class="modal-content">
                        
                            <!-- Modal Header -->
                            <div class="modal-header">
                            <h4 class="modal-title">Rechazar Requisición</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            
                            <!-- Modal body -->
                            <div class="modal-body">
                                    <div class="form-group">
                                            
                                            <textarea class="form-control" rows="5" id="comment" placeholder="Describa la razón por la cual, la requisición fué rechazada:"></textarea>
                                        </div> 
                            </div>
                            
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>

                            </div>
                            
                        </div>
                        </div>
                    </div>
                
            </div>

            </div>
        </main>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
      

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

    <?php require_once "./Parciales/Scripts.php"?>
    <script src="https://vuejs.org/js/vue.min.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script type="text/javascript" src="../js/JSmensajeEnvio.js"></script>
    <script type="text/javascript" src="../js/JSbuzon.js"></script>


</body>

</html>
