<?php require_once "./Parciales/ValidacionDeSesion.php" ?>
<?php require_once "./Parciales/Encabezado.php" ?>
<body id="page-top">

	<!-- Page Wrapper -->
	<div id="wrapper">

		<!-- Sidebar -->
		<?php require_once "./Parciales/MenuLateral.php" ?>
		<!-- End of Sidebar -->

		<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">

			<!-- Main Content -->
			<div id="content">

				<!-- Topbar -->
				<?php require_once "./Parciales/Top.php" ?>
				<!-- End of Topbar -->

				<!-- Begin Page Content -->
				<div class="container-fluid">


					<!-- Page Heading -->
				 <!--<h1 class=" text-center">Registro de Usuario</h1>
					
					
				 	<h5 class="  text-gray-800 text-center d-lg-block">CENTRO UNIVERSITARIO TECNOLÓGICO</h5>-->



				 	<!-- Content Row -->
				 	<div class="row d-flex justify-content-center">
					 <?php
					if($permisoActivo==1){
						echo '
				 		<!-- FORMULARIO aCTUALIZAR USUARIO -->
				 		<div class="col-sm-12 col-md-5">
						 <div class="progress">
						<div class="progress-bar bg-info progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 50%">Paso 1</div>
							
						</div>
				 			<div class="card shadow mb-4 trans">
				 				<div class="card-header py-3">
				 					<!--<img src="img/logo.png" class="img-responsive" width="50%">-->
				 					<h3 class="m-0 font-weight-bold text-primary">Actualizar usuario</h3>

				 				</div>
				 				<div class="card-body">

				 					<div class="d-sm-inline-block form-inline navbar-search">
				 						<div class="input-group text-center">
				 							<input type="text" class="form-control bg-light border-0 small" placeholder="Ingrese número de TH" aria-label="numeroTH" aria-describedby="basic-addon2" id="numeroTH" name="numeroTH" required>
				 							<div class="input-group-append">
				 								<button class="btn btn-primary" type="button" onclick="BuscarUsuario();">
				 									<i class="fas fa-search fa-sm"></i>
				 								</button>
				 							</div>
				 						</div>
				 					</div>

				 					<form action="PHPActualizarUsuarios.php" method="POST">
									 <input type="hidden"  id="IdSesion" name="IdSesion"></input>
				 						<div class="row">
											 <input type="hidden"  id="numeroTH2" name="numeroTH2"></input>
											 
											 <div class="form-group col-sm-12 col-md-6">

				 								<label for="nombres">Número TH *</label>
				 								<input type="text" class="form-control" id="numeroTHA" name="numeroTHA" placeholder="Número TH" required></input>

				 							</div>
				 							<div class="form-group col-sm-12 col-md-6">

				 								<label for="nombres">Nombres *</label>
				 								<input type="text" class="form-control" id="nombres" name="nombres" placeholder="Nombres" required>

				 							</div>
				 							<div class="form-group col-sm-12 col-md-6">

				 								<label for="apellidos">Apellidos *</label>
				 								<input type="text" class="form-control" id="apellidos" name="apellidos" placeholder="Apellidos" required>

				 							</div>

				 						</div>

				 						<div class="row">
											<div class="form-group col-sm-12 col-md-6">
												<label for="correo">Correo electrónico *</label>
												<input type="email" class="form-control" id="correo" name="correo" placeholder="Correo electrónico" required>
											</div>

											<div class="form-group col-sm-12 col-md-6">
												<label for="contrasenia">Contraseña *</label>
												<input type="password" class="form-control" id="contrasenia" name="contrasenia" placeholder="Contraseña" required>
											</div> 
										</div>


										<div class="row">
										<div class="form-group col-sm-12 col-md-6">
				 							<label for="activo">¿Está activo?:</label>       
				 							<select class="form-control" id="activo" name="activo" required>
				 								<option value="">--</option>
				 								<option value="1">SI</option>
				 								<option value="0">NO</option>
				 							</select>
				 						</div>


				 						<div class="form-group col-sm-12 col-md-6">
				 							<label for="idTipoUsuario">Categoría de usuario *</label>
				 							<select class="form-control" id="idTipoUsuario" name="idTipoUsuario" required>
				 								<option value="--">--</option>
				 								';
				 								$servername = "localhost";
				 								$username = "root";
				 								$password = "";
				 								$dbname = "sistemadeinventario";

										// Create connection
				 								$conn = new mysqli($servername, $username, $password, $dbname);
										// Check connection
				 								if ($conn->connect_error) {
				 									die("Connection failed: " . $conn->connect_error);
				 								} 

				 								$sql = "SELECT idTipoUsuario, Descripcion from tipousuario";
				 								$result= $conn->query($sql);

				 								if ($result->num_rows>0) {
				 									while($row=$result->fetch_assoc()){
				 										echo "<option value='".$row["idTipoUsuario"]."'>".$row["Descripcion"]."</option>";
				 									}
				 								} else {
				 									echo "<option> --Tabla vacia--</option>";
				 								}

				 								$conn->close();

				 								echo'
				 							</select>
				 						</div>
										</div>
				 						<button type="submit" class="btn btn-primary ">Actualizar usuario</button>

				 					</form>
				 				</div><!--card-body-->


				 			</div><!--card shadow mb-4 trans-->

				 		</div><!-- FORMULARIO aCTUALIZAR USUARIO -->

				 		<div class="col-sm-12 col-md-5">
						 <div class="card shadow mb-4 trans">
								<div class="card-header py-3">
				 					<!--<img src="img/logo.png" class="img-responsive" width="50%">-->
				 					<h3 class="m-0 font-weight-bold text-primary">Usuarios del sistema</h3>
				 				</div>
								 <div class="card-body">
								 <div class="table-responsive">
											<table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
												<thead>
													<tr>
														<th>Número TH</th>
														<th>Nombre</th>
														<th>Activo</th>
													</tr>
												</thead>
												<tbody>
													';
														$servername = "localhost";
														$username = "root";
														$password = "";
														$dbname = "sistemadeinventario";

														// Create connection
														$conn = new mysqli($servername, $username, $password, $dbname);
														// Check connection
														if ($conn->connect_error) {
															die("Connection failed: " . $conn->connect_error);
														}

														$sql = "SELECT NumeroTH, Nombres , Apellidos, Activo from usuarios";
														$result= $conn->query($sql);
														$act="";
														if ($result->num_rows>0) {
															while($row=$result->fetch_assoc()){
																$act = ( $row['Activo']==1 ) ? "Si" : "No"; 
																print "<tr>
																		<td>".$row['NumeroTH']."</td>".
																		"<td>".$row['Nombres']." ".$row['Apellidos']."</td>".
																		"<td>".$act."</td>".
																	"</tr>";
															}
														} else {
															echo "<option> --Tabla vacia--</option>";
														}

														$conn->close();

													echo'
												</tbody>
											</table>                         
									 </div>
								 </div>
							 </div>';
							}else{
								echo '
								<div class="col-lg-12 col-xl-7 ">
									<div class="card shadow mb-4 trans">
										<div class="card-header py-3">
											<!--<img src="img/logo.png" class="img-responsive" width="50%">-->
											<h3 class="m-0 font-weight-bold text-primary">No tiene acceso</h3>
		
										</div>
									</div>
								</div>
								';
							}
							?>
				 		</div>

				 		<!-- Donut Chart -->
				 	</div>
				 	<?php require_once "./Parciales/Scripts.php"?>
					 <script type="text/javascript" src="../js/JSActualizarUsuarios.js"></script>
					 <script type="text/javascript" src="../js/JSmensajeEnvio.js"></script>
					 <script type="text/javascript" src="../js/JSenviarIdSesion.js"></script>
				 </body>

				 </html>