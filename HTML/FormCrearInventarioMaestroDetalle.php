<?php  require_once "../config.php"; ?>
<?php require_once "./Parciales/Encabezado.php"; ?>


<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <?php require_once "./Parciales/MenuLateral.php" ?>
        <!-- End of Sidebar -->
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
                <?php require_once "./Parciales/Top.php" ?>
                <!-- End of Topbar -->
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Page Heading -->
                    <!--<h1 class=" text-center">Registro de Usuario</h1>
					<h5 class="  text-gray-800 text-center d-lg-block">CENTRO UNIVERSITARIO TECNOLÓGICO</h5>-->
                    <!-- Content Row -->
                    <div class="row justify-content-center">
                        <div id="formulario">
                            <!-- formularioVUE -->
                            <!-- caja -->
                            <div class="col-lg-12 col-xl-10 ">
                                <!-- ALERT -->
                                <div id="alert" class="alert alert-success alert-dismissible fade" role="alert">
                                    <strong id="msj">Texto de prueba!</strong>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                                <!-- alert -->
                                <div class="card shadow mb-4 trans">
                                    <div class="card-header py-3">

                                        <!--<img src="img/logo.png" class="img-responsive" width="50%">-->
                                        <h3 class="m-0 font-weight-bold text-primary">Registro de inventario detalles
                                        </h3>

                                    </div>
                                    <div class="card-body">
                                        <form @submit.prevent="insertMaestro">
                                            <div class="row ">
                                                <div class="form-group col-sm-12 col-md-4">
                                                    <label> Proveedor *</label>
                                                    <select name="idProveedor" id="idProveedor" v-model="idProveedor"
                                                        name="idProveedor" class="custom-select form-control shadow-sm">
                                                        <option selected disabled value="0">Seleccionar...</option>
                                                        <?php
															$servername = "localhost"; $username = "root"; $password = ""; $dbname = "sistemadeinventario";
															$conn = new mysqli($servername, $username, $password, $dbname);
															if ($conn->connect_error) { die("Connection failed: " . $conn->connect_error); }
																$sql = "SELECT idProveedor, Nombre from Proveedores";
																$result= $conn->query($sql);
                                                                    if ($result->num_rows>0) {
                                                                            while($row=$result->fetch_assoc()){
                                                                            print "<option value='{$row["idProveedor"]}'>{$row["Nombre"]}</option>";
                                                                        }
															}else{
																echo "<option> --Tabla vacia / no existe--</option>";
                                                            }
                                                            $conn->close();
															?>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-12 col-md-4">
                                                    <label for="nombres">Número de factura *</label>
                                                    <input type="text" class="form-control shadow-sm" id="numeroFactura"
                                                        v-model="numeroFactura" name="numeroFactura" placeholder="20978"
                                                        pattern="^[0-9]+$" required>
                                                </div>
                                                <div class="form-group col-xs-12 col-sm-12 col-md-4">
                                                    <label for="fecha">Fecha de compra *</label>
                                                    <input type="date" class=" form-control shadow-sm" id="fechaCompra"
                                                        v-model="fechaCompra" name="fechaCompra" required>
                                                </div>
                                                <div v-if="!estadoMaestro" class="form-group">
                                                    <button  class="btn btn-primary shadow-sm"
                                                        type="submit">Agregar</button>
                                                </div>
                                            </div>
                                        </form><!-- maestro -->
                                        <br>
                                        <!-- formDetalle -->
                                        <div id="formDetalle" v-if="estadoMaestro">
                                            <div class="form-group">
                                                <div class="table-responsive">
                                                    <table class="table table-striped responsive-table" id="dataTable"
                                                        width="100%" cellspacing="0">
                                                        <thead class="">
                                                            <tr>
                                                                <th>Id</th>
                                                                <th>Descripción</th>
                                                                <th>Precio</th>
                                                                <th>Cantidad</th>
                                                                <th>Subtotal</th>
                                                                <th>ISV</th>
                                                                <th>Acciones</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr v-for="(item, index) in articulos">
                                                                <td>{{item.idArticulo}}</td>
                                                                <td>{{ item.descripcion }}</td>
                                                                <td>L. {{ item.precio }}</td>
                                                                <td>{{ item.cantidad }}</td>
                                                                <td>L. {{ (item.subtotal).toFixed(2) }}</td>
                                                                <td>
                                                                    <h5 v-if="item.isv" class='far fa-check-circle'>
                                                                    </h5>
                                                                    <h5 v-if="!item.isv" class='far fa-times-circle'>
                                                                    </h5>
                                                                </td>
                                                                <td>
                                                                    <button type="button"
                                                                        class="btn btn-primary btn-circle btn-md"
                                                                        title="Editar artìculo"
                                                                        @click="showEditArticle(index)">
                                                                        <i class="fas fa-pencil-alt"></i>
                                                                    </button> /
                                                                    <button type="button"
                                                                        class="btn btn-danger btn-circle btn-md"
                                                                        title="Eliminar artículo"
                                                                        @click="deleteArticulo(index)">
                                                                        <i class="fas fa-trash"></i>
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                                <!-- MOdal - Actualizar articulos -->
                                                <div class="modal fade" id="editArticulo" tabindex="-1" role="dialog"
                                                    aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                                    <form @submit.prevent="updateArticulo">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title m-0 font-weight-bold text-primary"
                                                                        id="exampleModalLongTitle">Actualizar Articulo
                                                                    </h5>
                                                                    <button type="button" class="close"
                                                                        data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="row d-flex-justify-content-center">
                                                                        <div class="col-sm-12 col-md-3">
                                                                            <label for="fecha">Articulo *</label>
                                                                            <select name="articulos" id="idArticulo"
                                                                                v-model="articuloSeleccionado"
                                                                                class="custom-select form-control">
                                                                                <option selected>Selected </option>
                                                                                <option v-for="item in DBArticulos"
                                                                                    :value="item.IdArticulo">
                                                                                    {{ item.Descripcion }}</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-sm-12 col-md-3">
                                                                            <label for="fecha">Cantidad *</label>
                                                                            <input type="number" class="form-control"
                                                                                @keyup="keyupEditArticle()"
                                                                                v-model="articuloActual.cantidad"
                                                                                name="cantidad" required>
                                                                        </div>
                                                                        <div class="col-sm-12 col-md-3">
                                                                            <label for="fecha">Precio *</label>
                                                                            <input type="text" class="form-control"
                                                                                @keyup="keyupEditArticle()"
                                                                                v-model="articuloActual.precio"
                                                                                name="precio" required>
                                                                        </div>
                                                                        <div class="col-sm-12 col-md-3">
                                                                            <label for="subtotal">SubTotal</label>
                                                                            <input type="text" class="form-control"
                                                                                disabled
                                                                                v-model="articuloActual.subtotal"
                                                                                name="subtoal" required>
                                                                            <!--<button type="submit" class="btn btn-primary" @click="updateArticulo" >Actualizar</button>-->
                                                                        </div>
                                                                    </div>
                                                                    <div class="row d-flex-justify-content-center">
                                                                        <div class="custom-control custom-checkbox my-1 mr-sm-2">
                                                                            <input class="custom-control-input" type="checkbox"
                                                                                id="checkbox" v-model="articuloActual.isv">
                                                                            <label class="custom-control-label" for="checkbox">Con
                                                                                ISV</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="sumit" class="btn btn-success"
                                                                        data-dismiss="modal">Actualizar</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <!-- MOdal - Actualizar articulos -->

                                            </div>

                                            <div class="form-group">
                                                <div class="d-flex justify-content-around p-3">
                                                    <h5 class="font-weight-bold">SubTotal: L. {{ sumaSubTotal }}</h5>

                                                    <h5 class="font-weight-bold">Total: L. {{ sumaTotal  }} </h5>

                                                </div>
                                            </div>

                                            <div id="" class="form-group">
                                                <form @submit.prevent="addArticulo">
                                                    <div class="row d-flex-justify-content-center shadow-sm rounded bg-white p-3">
                                                        <div class="col-sm-12 col-md-3">
                                                            <label for="fecha">Articulos *</label>
                                                            <select name="articulos" id="idArticulo"
                                                                v-model="articuloSeleccionado"
                                                                class="custom-select form-control">
                                                                <option disabled value="">Please select one</option>
                                                                <option v-for="item in DBArticulos" :value="item">
                                                                    {{ item.Descripcion }}</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-12 col-md-3">
                                                            <label for="fecha">Cantidad *</label>
                                                            <input type="number" class="form-control" v-model="cantidad"
                                                                name="cantidad" required>
                                                        </div>
                                                        <div class="col-sm-12 col-md-3">
                                                            <label for="fecha">Precio *</label>
                                                            <input type="text" class="form-control" v-model="precio"
                                                                name="precio" required>
                                                    
                                                                
                                                        </div>
                                                        <div class="col-sm-12 col-md-3">
                                                                    <div class="custom-control custom-checkbox my-1 mr-sm-2">
                                                                            <input class="custom-control-input" type="checkbox"
                                                                                id="checkbox" v-model="conIsv">
                                                                            <label class="custom-control-label" for="checkbox">Con
                                                                                ISV</label>
                                                                        </div>
                                                            <input type="hidden" name="subTotal" v-model="subTotal">
                                                            <input type="hidden" name="total" v-model="total">
                                                            <input type="hidden" name="idMaestro" v-model="idMaestro">
                                                            <button type="submit"
                                                                class="btn btn-primary">Agregar</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                            <button type="submit" class="btn btn-primary"
                                                @click="registrarDetalle">Registrar</button>

                                        </div><!-- formDetalle-->
                                    </div>
                                </div>
                            </div> <!-- caja -->

                        </div> <!-- formularioVUE-->
                    </div>
                    <!-- Donut Chart -->
                </div>



                <?php require_once "./Parciales/Scripts.php"?>
                <script src="https://vuejs.org/js/vue.min.js"></script>
                <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
                <script type="text/javascript" src="../js/JSmensajeEnvio.js"></script>
                <script type="text/javascript" src="../js/JSCrearInventarioDetalle.js"></script>


</body>

</html>