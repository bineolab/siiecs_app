<?php require_once "./Parciales/ValidacionDeSesion.php" ?>
<?php require_once "./Parciales/Encabezado.php" ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="../js/JSmensajeEnvio.js"></script>
<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once "./Parciales/MenuLateral.php" ?>
          <!-- End of Sidebar -->

          <!-- Content Wrapper -->
          <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

              <!-- Topbar -->
              <?php require_once "./Parciales/Top.php" ?>
              <!-- End of Topbar -->

              <!-- Begin Page Content -->
              <div class="container-fluid">


                <!-- Page Heading -->
         <!--<h1 class=" text-center">Registro de Usuario</h1>
          
          
          <h5 class="  text-gray-800 text-center d-lg-block">CENTRO UNIVERSITARIO TECNOLÓGICO</h5>-->
          
          
          
          <!-- Content Row -->
          <div class="row justify-content-center">
          <?php
			if($permisoActivo==1){
			echo '
            
            <div class="col-lg-12 col-xl-7  ">
              <div class="card shadow mb-4 trans">
                <div class="card-header py-3">
                  <!--<img src="img/logo.png" class="img-responsive" width="50%">-->
                  <h3 class="m-0 font-weight-bold text-primary">Crear lugar práctica recomendado</h3>
                </div>


                <!--Formulario-->
                <div class="card-body">
                  <form action="PHPCrearLugarPractica.php" method="POST">
				  <input type="hidden"  id="IdSesion" name="IdSesion"></input>
                  <div class="form-group">

                  <div class="row">
                  <div class="form-group col-sm-12 col-md-6">
                        <label for="descripcion">Nombre</label>
                        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Descripción del lugar práctica">
                      </div>
                  <div class="form-group col-sm-12 col-md-6">
                        <label for="descripcion">Descripción</label>
                        <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Descripción del lugar práctica">
                      </div>
    
                      
                        </div>
             
            <button type="submit" class="btn btn-primary ">Guardar</button>
   
          </form>
        </div>
      </div>';
    }else{
        echo '
        <div class="col-lg-12 col-xl-7 ">
          <div class="card shadow mb-4 trans">
            <div class="card-header py-3">
              <!--<img src="img/logo.png" class="img-responsive" width="50%">-->
              <h3 class="m-0 font-weight-bold text-primary">No tiene acceso</h3>
    
            </div>
          </div>
        </div>
        ';
    }
    ?>

    </div>

    <!-- Donut Chart -->
    
  </div>

  <?php require_once "./Parciales/Scripts.php"?>
 <script type="text/javascript" src="../js/JSenviarIdSesion.js"></script>
</body>

</html>