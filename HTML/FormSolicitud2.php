<?php require_once "./Parciales/Encabezado.php" ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="../js/JSmensajeEnvio.js"></script>



<script type="text/javascript" src="../js/JSActualizarTelefono1.js"></script>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once "./Parciales/MenuLateral.php" ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php require_once "./Parciales/Top.php" ?>
        <!-- End of Topbar -->

        
        <!-- Begin Page Content -->
        
        <div class="row justify-content-center">


          <div class="col-lg-12 col-xl-7  " >
            <div class="card shadow mb-4 trans" id ="FormMaestro">
              <div class="card-header py-3">
                <!--<img src="img/logo.png" class="img-responsive" width="50%">-->
                <h3 class="m-0 font-weight-bold text-primary">Solicitud </h3>
                <h6 class="m-0 font-weight-bold text-primary">información general</h6>
              </div>
              
              <div class="card-body">

                <div class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                  <div class="input-group">
                   
                    <div class="input-group-append">
                     
                    </div>
                  </div> 
                </div>
                <form action="PHPCrearSolicitud.php" method="GET">
                   <input type="hidden"  id="IdSesion" name="IdSesion"></input>
                <div class="row">
                  <div class="form-group col-sm-12 col-md-6">


                  

                   <label for="numeroTh">NúmeroTH *</label>
                   <?php

                            $servername = "localhost";
                            $username = "root";
                            $password = "";
                            $dbname = "sistemadeinventario";

                          
                          
                        // Create connection
                            $conn = new mysqli($servername, $username, $password, $dbname);
                        // Check connection

                        // Check connection
                        if ($conn->connect_error) {
                          die("Connection failed: " . $conn->connect_error);
                        } 

                        $sql = "SELECT NumeroTH FROM sesiones a WHERE a.IdSesion=".$_GET["IdSesion"];
                        $result= $conn->query($sql);

                         if ($result->num_rows>0) {
                             while($row=$result->fetch_assoc()){
                                 echo '<input type="text" class="form-control" id="numeroTh" name="numeroTh" placeholder="Número TH" required disabled value="'.$row["NumeroTH"].'">';
                             }
                        } else {
                             echo '<input type="text" class="form-control" id="numeroTh" name="numeroTh" placeholder="Número TH" required disabled value="Usuario no logeado">';
                       }


                   ?>
                   
                 </div>

                 <div class="form-group col-sm-12 col-md-6">
                   <label for="fechasolic">Fecha de solicitud*</label>
                   <input type="date" class="form-control" id="fechasolic" name="fechasolic" placeholder="Fecha" required>
                 </div>
               </div>
               
              <hr>
     
              <div class="row">
              <div class="form-group col-sm-12 col-md-7">
                   <label for="idclase">Nombre Asignatura*</label>
              <select class="form-control" id="idclase" name="idclase">
                    <?php
                    $servername = "localhost";
                    $username = "root";
                    $password = "";
                    $dbname = "sistemadeinventario";

                                            // Create connection
                    $conn = new mysqli($servername, $username, $password, $dbname);
                                            // Check connection
                    if ($conn->connect_error) {
                      die("Connection failed: " . $conn->connect_error);
                    } 

                    $sql = "SELECT IdClase, NombreClase from clases";
                    $result= $conn->query($sql);

                    if ($result->num_rows>0) {
                      while($row=$result->fetch_assoc()){
                        echo "<option value='".$row["IdClase"]."'>".$row["NombreClase"]."</option>";
                      }
                    } else {
                      echo "<option> --Tabla vacia--</option>";
                    }

                    $conn->close();

                    ?>
                  </select>
                  </div>
                  <div class="form-group col-sm-12 col-md-4">
                    <label for="seccion">Seccion*</label>
                    <input type="text" class="form-control" id="seccion" name="seccion" placeholder=" Seccion" required>
                  </div>
                  </div>
                  <div class="row">
                      <div class="form-group col-sm-12 col-md-10">
                    <label for="nombrepractica">Nombre de la práctica*</label>
                    <input type="text" class="form-control" id="nombrepractica" name="nombrepractica" placeholder="Nombre Practica" required>
                  </div>
                  </div>
                  <div class="row">
              <!--<div class="form-group col-sm-12 col-md-7">
                   <label for="idlugar">Lugar de práctica recomendado*</label>
              <select class="form-control" id="idlugar" name="idlugar">
                    <?php
                    /*$servername = "localhost";
                    $username = "root";
                    $password = "";
                    $dbname = "sistemadeinventario";

                                            // Create connection
                    $conn = new mysqli($servername, $username, $password, $dbname);
                                            // Check connection
                    if ($conn->connect_error) {
                      die("Connection failed: " . $conn->connect_error);
                    } 

                    $sql = "SELECT IdLugarPractica, Nombre from LugarPractica";
                    $result= $conn->query($sql);

                    if ($result->num_rows>0) {
                      while($row=$result->fetch_assoc()){
                        echo "<option value='".$row["IdLugarPractica"]."'>".$row["Nombre"]."</option>";
                      }
                    } else {
                      echo "<option> --Tabla vacia--</option>";
                    }

                    $conn->close();

                    */?>
                  </select>
                  </div>-->
                  </div>
                  
                

                  <div class="row">
                      <div class="form-group col-sm-12 col-md-4">
                    <label for="horaclase">Hora Clase*</label>
                    <input type="time" class="form-control" id="horaclase" name="horaclase" placeholder="Hora Inicio" required>
                  </div>
                  <div class="form-group col-sm-12 col-md-4">
                    <label for="horainicio">Hora inicio de practica*</label>
                    <input type="time" class="form-control" id="horainicio" name="horainicio" placeholder="Hora Inicio" required>
                  </div>
                  <div class="form-group col-sm-12 col-md-4">
                    <label for="horafin">Hora fin de practica*</label>
                    <input type="time" class="form-control" id="horafin" name="horafin" placeholder="Hora Fin" required>
                  </div>
                  <div class="form-group col-sm-12 col-md-6">
                   <label for="fechasolic2">Fecha para la solicitud*</label>
                   <input type="date" class="form-control" id="fechasolic2" name="fechasolic2" placeholder="Fecha" required>
                 </div>
               </div>
                  </div>
                  <div class="card-header py-3" style="margin-left:80%;">
          <button type="submit" class="btn btn-primary ">Siguiente</button>  
        </div>
             </form>
           </div>



						


          <!--Formuylario detalle--> 
          <form action="PHPCrearDetalle.php" method="GET" id ="FormDet">   
          <input type="hidden"  id="IdSesion2" name="IdSesion2"></input>
             <input type="hidden" id="idSolicitud2" name="idSolicitud2"> </input>
          
          
              <!--Form detalles de la solicitud-->
              <div class="card shadow mb-4 trans">
                      <div class="card-header py-3">
                        <!--<img src="img/logo.png" class="img-responsive" width="50%">-->
                        <h6 class="m-0 font-weight-bold text-primary">Detalles de  solicitud</h6>

                      </div>

                      <!-- Tabla de detalle-->
                      <div class="form-group col-sm-12 col-md-12" >
                        <table class='table'  id="tablaprueba">
                          <thead>
                            <tr>
                              <th scope='col'>Articulo</th>
                              <th scope='col' >Cantidad solicitada</th>
                            </tr>
                            <tr>
                              <td>
                                <select class="form-control" id="IdArticulo" name="IdArticulo" required>
                                  <option value="">Selecionar artículo</option>
                                  <?php
                                  $servername = "localhost";
                                  $username = "root";
                                  $password = "";
                                  $dbname = "sistemadeinventario";

                                                          // Create connection
                                  $conn = new mysqli($servername, $username, $password, $dbname);
                                                          // Check connection
                                  if ($conn->connect_error) {
                                    die("Connection failed: " . $conn->connect_error);
                                  } 

                                  $sql = "SELECT   IdArticulo, 
                                  Descripcion
                                  from    articulo a 
                                  where   not exists (select * 
                                                          from detallesolicitud b 
                                                          where a.IdArticulo = b.IdArticulo 
                                                          and b.IdSolicitud = ".$_GET["IdSolicitud"].")  
                                  ORDER BY Descripcion asc";
                                  $result= $conn->query($sql);

                                  if ($result->num_rows>0) {
                                    while($row=$result->fetch_assoc()){
                                      echo "<option value='".$row["IdArticulo"]."'>".$row["Descripcion"]."</option>";
                                    }
                                  } else {
                                    echo "<option> --Tabla vacia--</option>";
                                  }

                                  $conn->close();

                                  ?>
                                </select>
                              </td>
                              <td> 
                                <input type="text" class="form-control bg-light  small" placeholder="00" aria-label="numeroTH" aria-describedby="basic-addon2" id="CantidadSolicitada" name="CantidadSolicitada" required>
                              </td>
                            </tr>
                          </thead>
                          
                          
                          <tbody id="tablaBody">

                            
                          </tbody>
                        </table>
                          </form>
                          <button type="submit" class="btn btn-primary mr-2">Agregar</button> 
                          <br><br><hr>

                    

                    <!-- Tabla Descripcion de las solicitudes-->
                  <div class="card shadow mb-4 trans" id ="FormDet2">
								<div class="card-header py-3">
				 					<!--<img src="img/logo.png" class="img-responsive" width="50%">-->
				 					<h3 class="m-0 font-weight-bold text-primary">Editar artículos de la solicitud</h3>
				 				</div>
								 <div class="card-body">
								 <div class="table-responsive">
											<table  class="table table-striped" id="dataTable2" width="100%" cellspacing="0">
												<thead>
													<tr>
														<th>Det. Sol.</th>
                            <th>Articulo</th>
														<th>Cantidad Solicitada</th>
														<th>Acción</th>
													</tr>
												</thead>
												<tbody>
                          <?php
                           
														$servername = "localhost";
														$username = "root";
														$password = "";
														$dbname = "sistemadeinventario";

														// Create connection
														$conn = new mysqli($servername, $username, $password, $dbname);
														// Check connection
														if ($conn->connect_error) {
															die("Connection failed: " . $conn->connect_error);
														}

														$sql = "SELECT CantidadSolicitada, B.Descripcion, IdDetalleSolicitud  from detallesolicitud A
                            INNER JOIN  Articulo B on  A.IdArticulo= B.IdArticulo
                            where IdSolicitud=".$_GET["IdSolicitud"];
														$result= $conn->query($sql);
														
														if ($result->num_rows>0) {
															while($row=$result->fetch_assoc()){
												
                                print "<tr>
                                <td class='nr'>".$row['IdDetalleSolicitud']."</td>".
																		"<td>".$row['Descripcion']."</td>".
                                    "<td>".$row['CantidadSolicitada']." </td>".
                                   
                                      "<td>
                                      <button type='button' class='btn btn-primary  btn-sm use-address'>Editar</button>
                                      <a href='Eliminar.php?no=".$row['IdDetalleSolicitud']."&IdSolicitud=".$_GET['IdSolicitud']."&IdSesion=".$_GET["IdSesion"]."' style='color: white;'><button type='button' class='btn btn-danger  btn-sm use-address1'>Borrar</a></button>
                                     
                                    </td>".
																	"</tr>";
															}
														} else {
															echo "<option> </option>";
														}

														$conn->close();

													?>
												</tbody>
											</table> 
                                              
									 </div>
								 </div>
							 </div>
				 		</div>


            


  <!-- Trigger the modal with a button -->
  <div class="modal fade" id="ModalForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title m-0 font-weight-bold text-primary" id="exampleModalLongTitle">Editar Solicitud:</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                
                <form action="modif_prod2.php">
                <input type="hidden"  id="IdSesion3" name="IdSesion3"></input>
                    <input type="hidden" id="IdDetalleSolicitud" name="IdDetalleSolicitud22" value="<?php echo $_GET['IdDetalleSolicitud'] ?>">
                    <input type="hidden" id="IdSolicitud" name="IdSolicitud22" value="<?php echo $_GET['IdSolicitud'] ?>">
                    
                    <div class="form-group col-sm-12 col-md-6">
                 
                   <input type="num" class="form-control" id="CantidadSolicitada" name="CantidadSolicitada22" placeholder="Cantidad"  required>
                  <br> <button type="submit" class="btn btn-primary ">Modificar</button>
                 </div>
 
                </form>

              </div>
              <div class="modal-footer">
                
       
                
              </div>
            </div>
          </div>
        </div>
        


                            <div class="row d-flex justify-content-center">
                            
                            <?php
       

                                        echo "<a href='FormSolicitud2.php?IdSesion=".$_GET['IdSesion']."' class='btn btn-primary col-sm-12 col-md-6'>Terminar requisición</a>"


                                  ?>
                            </div>

<!-- /.container-fluid -->

<!-- Trigger the modal with a button -->

                <form action="Eliminar.php" method="GET" >
                <input type="hidden"  id="IdSesion4" name="IdSesion4"></input>
                    <input type="hidden" id="IdDetalleSolicitud" name="IdDetalleSolicitud22" value="<?php echo $_GET['IdDetalleSolicitud'] ?>">
                    <input type="hidden" id="IdSolicitud23" name="IdSolicitud23" value="<?php echo $_GET['IdSolicitud'] ?>">
                    <div class="form-group col-sm-12 col-md-6">
                 
                   <input type="hidden" class="form-control" id="CantidadSolicitada" name="CantidadSolicitada22" placeholder="Cantidad"  required>
                 </div>
                </form>
             

<!-- /.container-fluid -->

  
<!-- /.container-fluid -->


</div>
<!-- End of Main Content -->

<!-- Footer -->
<footer class="sticky-footer bg-white">
  <div class="container my-auto">
    <div class="copyright text-center my-auto">
      <span>Copyright &copy; Your Website 2019</span>
    </div>
  </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->
<script>

$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null) {
       return null;
    }
    return decodeURI(results[1]) || 0;
}

  var validar =$.urlParam('IdSolicitud');

  if (validar == null ){
    $("#FormDet").hide();
    $("#FormDet2").hide();   // To hide

  }else{

    $("#idSolicitud2").val(validar);
    $("#FormMaestro").hide();
   

  }

</script>
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>

<script>

$(".use-address").click(function() {
    var $row = $(this).closest("tr");    
    var $text = $row.find(".nr").text(); 
    
    $('#IdDetalleSolicitud').val($text);
    $('#CantidadSolicitada').val($text);
    $('#ModalForm').modal('show');
    

});

</script>

<script>
$(".use-address1").click(function() {
    var $row = $(this).closest("tr");    
    var $text = $row.find(".nr").text(); 

    $('#ModalForm2').modal('show');
});
</script>

<script>
function ParametrosURL(){
    
          document.getElementById("IdSolicitud23").value=document.getElementById("idSolicitud2").value;
          console.log(IdSolicitud23.value);
          document.getElementById("IdSesion2").value=document.getElementById("IdSesion").value;
          document.getElementById("IdSesion3").value=document.getElementById("IdSesion").value;
          document.getElementById("IdSesion4").value=document.getElementById("IdSesion").value;

      
  }
  ParametrosURL();
</script>

<?php require_once "./Parciales/Scripts.php"?>

<script type="text/javascript" src="../js/JSActualizarDetalleSolicitud.js"></script>
<script type="text/javascript" src="../js/JSenviarIdSesion.js"></script>



</body>

</html>