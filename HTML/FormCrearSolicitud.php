<?php require_once "./Parciales/ValidacionDeSesion.php" ?>
<?php require_once "./Parciales/Encabezado.php" ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="../js/JSmensajeEnvio.js"></script>



<script type="text/javascript" src="../js/JSActualizarTelefono1.js"></script>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once "./Parciales/MenuLateral.php" ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php require_once "./Parciales/Top.php" ?>
        <!-- End of Topbar -->

        
        <!-- Begin Page Content -->
        <form action="PHPCrearSolicitud.php" method="GET">

        <div class="row justify-content-center">

        <?php
					if($permisoActivo==1){
						echo '
          <div class="col-lg-12 col-xl-7  ">
            <div class="card shadow mb-4 trans">
              <div class="card-header py-3">
                <!--<img src="img/logo.png" class="img-responsive" width="50%">-->
                <h3 class="m-0 font-weight-bold text-primary">Solicitud </h3>
                <h6 class="m-0 font-weight-bold text-primary">información general</h6>
              </div>
              
              <div class="card-body">

                <div class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                  <div class="input-group">
                   
                    <div class="input-group-append">
                     
                    </div>
                  </div> 
                </div>
                
                <div class="row">
                  <div class="form-group col-sm-12 col-md-6">
                   <label for="numeroTh">NúmeroTH *</label>
                   <input type="text" class="form-control" id="numeroTh" name="numeroTh" placeholder="númeroTH" required>
                 </div>

                 <div class="form-group col-sm-12 col-md-6">
                   <label for="fechasolic">Fecha para la solicitud*</label>
                   <input type="date" class="form-control" id="fechasolic" name="fechasolic" placeholder="Fecha" required>
                 </div>
               </div>
               
              <hr>
              <div class="row">
              <div class="form-group col-sm-12 col-md-7">
                   <label for="idclase">Nombre Asignatura*</label>
              <select class="form-control" id="idclase" name="idclase">';
                    
                    $servername = "localhost";
                    $username = "root";
                    $password = "";
                    $dbname = "sistemadeinventario";

                                            // Create connection
                    $conn = new mysqli($servername, $username, $password, $dbname);
                                            // Check connection
                    if ($conn->connect_error) {
                      die("Connection failed: " . $conn->connect_error);
                    } 

                    $sql = "SELECT IdClase, NombreClase from clases";
                    $result= $conn->query($sql);

                    if ($result->num_rows>0) {
                      while($row=$result->fetch_assoc()){
                        echo "<option value='".$row["IdClase"]."'>".$row["NombreClase"]."</option>";
                      }
                    } else {
                      echo "<option> --Tabla vacia--</option>";
                    }

                    $conn->close();

                    echo '
                  </select>
                  </div>
                  <div class="form-group col-sm-12 col-md-6">
                    <label for="seccion">Seccion*</label>
                    <input type="text" class="form-control" id="seccion" name="seccion" placeholder=" Seccion" required>
                  </div>
                  </div>

                  <div class="row">
                      <div class="form-group col-sm-12 col-md-10">
                    <label for="nombrepractica">Nombre de la practica*</label>
                    <input type="text" class="form-control" id="nombrepractica" name="nombrepractica" placeholder="Nombre Practica" required>
                  </div>
                  </div>

                  <div class="row">
                      <div class="form-group col-sm-12 col-md-6">
                    <label for="horainicio">Hora inicio de practica*</label>
                    <input type="time" class="form-control" id="horainicio" name="horainicio" placeholder="Hora Inicio" required>
                  </div>
                  <div class="form-group col-sm-12 col-md-6">
                    <label for="horafin">Hora fin de practica*</label>
                    <input type="time" class="form-control" id="horafin" name="horafin" placeholder="Hora Fin" required>
                  </div>
                  </div>
                  <div class="card-header py-3" style="margin-left:80%;">
          <button type="submit" class="btn btn-primary ">Siguiente</button>  
        </div>
             </form>
           </div>



						

          <!--Formuylario detalle--> 
         
          <form action="PHPCrearDetalle.php" method="GET" id ="FormDet">   
          <input type="hidden"  id="IdSesion" name="IdSesion"></input>
                      <input type="hidden" id="idSolicitud2" name="idSolicitud2"> </input>
          
          
              <!--Form detalles de la solicitud-->
              <div class="card shadow mb-4 trans">
                      <div class="card-header py-3">
                        <!--<img src="img/logo.png" class="img-responsive" width="50%">-->
                        <h6 class="m-0 font-weight-bold text-primary">Detalles de  solicitud</h6>

                      </div>

                      <!-- Tabla de detalle-->
                      <div class="form-group col-sm-12 col-md-12" >
                        <table class="table"  id="tablaprueba">
                          <thead>
                            <tr>
                              <th scope="col">Articulo</th>
                              <th scope="col" >Cantidad solicitada</th>
                            </tr>
                            <tr>
                              <td>
                                <select class="form-control" id="IdArticulo" name="IdArticulo">
                                  ';
                                  $servername = "localhost";
                                  $username = "root";
                                  $password = "";
                                  $dbname = "sistemadeinventario";

                                                          // Create connection
                                  $conn = new mysqli($servername, $username, $password, $dbname);
                                                          // Check connection
                                  if ($conn->connect_error) {
                                    die("Connection failed: " . $conn->connect_error);
                                  } 

                                  $sql = "SELECT IdArticulo, Descripcion from articulo";
                                  $result= $conn->query($sql);

                                  if ($result->num_rows>0) {
                                    while($row=$result->fetch_assoc()){
                                      echo "<option value='".$row["IdArticulo"]."'>".$row["Descripcion"]."</option>";
                                    }
                                  } else {
                                    echo "<option> --Tabla vacia--</option>";
                                  }

                                  $conn->close();

                                  echo '
                                </select>
                              </td>
                              <td> 
                                <input type="text" class="form-control bg-light  small" placeholder="00" aria-label="numeroTH" aria-describedby="basic-addon2" id="CantidadSolicitada" name="CantidadSolicitada" >
                              </td>
                            </tr>
                          </thead>
                          
                          <tbody id="tablaBody">

                            
                          </tbody>
                        </table>
                          </form>
                          <button type="submit" class="btn btn-primary mr-2">Agregar</button> 
                          <br><br><hr>

                    
                    <!-- Tabla Descripcion de las solicitudes-->
                  <div class="card shadow mb-4 trans" id ="FormDet2">
								<div class="card-header py-3">
				 					<!--<img src="img/logo.png" class="img-responsive" width="50%">-->
				 					<h3 class="m-0 font-weight-bold text-primary">Articulos de la solicitud</h3>
				 				</div>
								 <div class="card-body">
								 <div class="table-responsive">
											<table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
												<thead>
													<tr>
														<th>Articulo</th>
														<th>Cantidad Solicitada</th>
														<th>Acción</th>
													</tr>
												</thead>
												<tbody>
													';
														$servername = "localhost";
														$username = "root";
														$password = "";
														$dbname = "sistemadeinventario";

														// Create connection
														$conn = new mysqli($servername, $username, $password, $dbname);
														// Check connection
														if ($conn->connect_error) {
															die("Connection failed: " . $conn->connect_error);
														}

														$sql = "SELECT CantidadSolicitada, B.Descripcion from detallesolicitud A
                            INNER JOIN  Articulo B on  A.IdArticulo= B.IdArticulo
                            where IdSolicitud=".$_GET["IdSolicitud"];
														$result= $conn->query($sql);
														
														if ($result->num_rows>0) {
															while($row=$result->fetch_assoc()){
												
																print "<tr>
																		<td>".$row['Descripcion']."</td>".
																		"<td>".$row['CantidadSolicitada']." </td>".
                                      "<td> <div class='btn-group btn-group-sm'>
                                      <button type='button' class='btn btn-primary'>Editar</button>
                                      <button type='button' class='btn btn-danger'>Borrar</button>
                                    </div> </td>".
																	"</tr>";
															}
														} else {
															echo "<option> </option>";
														}

														$conn->close();

													echo '
												</tbody>
											</table>                         
									 </div>
								 </div>
               </div>';
              }else{
                echo '
                <div class="col-lg-12 col-xl-7 ">
                  <div class="card shadow mb-4 trans">
                    <div class="card-header py-3">
                      <!--<img src="img/logo.png" class="img-responsive" width="50%">-->
                      <h3 class="m-0 font-weight-bold text-primary">No tiene acceso</h3>
    
                    </div>
                  </div>
                </div>
                ';
              }
              ?>
               
				 		</div>
            </p>
         


<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Footer -->
<footer class="sticky-footer bg-white">
  <div class="container my-auto">
    <div class="copyright text-center my-auto">
      <span>Copyright &copy; Your Website 2019</span>
    </div>
  </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->
<script>

$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null) {
       return null;
    }
    return decodeURI(results[1]) || 0;
}

  var validar =$.urlParam('IdSolicitud');

  if (validar == null ){
    $("#FormDet").hide();
    $("#FormDet2").hide();   // To hide

  }else{

    $("#idSolicitud2").val(validar);

  }

</script>
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>



<?php require_once "./Parciales/Scripts.php"?>
<script type="text/javascript" src="../js/JSAgregarDetalle.js"></script>
<script type="text/javascript" src="../js/JSenviarIdSesion.js"></script>


</body>

</html>