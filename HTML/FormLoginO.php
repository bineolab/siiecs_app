<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SB Admin 2 - Login</title>

  <!-- Custom fonts for this template-->
  <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../css/sb-admin-2.min.css" rel="stylesheet">

 
  <script type="text/javascript">
    function login(){
      var xhttp, xmlDoc, txt, x, i, a, b,conteo;
      xhttp = new XMLHttpRequest();
	
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          xmlDoc = this.responseXML;
          txt = "";
          a = xmlDoc.getElementsByTagName("conteo");
			
          for (i = 0; i < a.length; i++) {
			conteo=a[i].childNodes[0].nodeValue;		       
          }
		  
		  //alert (conteo);
		  
		  if(conteo == 1){
			 window.location.assign("FormCrearUsuarios.php");
		  }else{
			  alert("Usuario Incorrecto");
		  }
		  
          
        }
      };
	  

      xhttp.open("GET", "../SERVICIOS/XMLlogin.php?usuario="+document.getElementById("usuario").value+"&password="+document.getElementById("password").value, true);
      xhttp.send();
	  
	  
	  
	  
	  
    }

    
  </script>
</head>

<body class="bg-gradient-primary">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">¡Bienvenido a SIIECS!</h1>
                  </div>
                  <form class="user">
                    <div class="form-group">
                      <input type="text" class="form-control form-control-user" placeholder="Ingrese su número de TH." id="usuario" name="usuario" required>
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control form-control-user" placeholder="Ingrese su password" id="password" name="password" required>
                    </div>
                
    
                    <button type="submit" onclick="login();" class="btn btn-primary btn-user btn-block">Aceptar</button>
                    <hr>
                    
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <?php require_once "./Parciales/Scripts.php"?>

</body>

</html>
