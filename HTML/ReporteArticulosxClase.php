<?php require_once "./Parciales/Encabezado.php" ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="../js/JSmensajeEnvio.js"></script>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once "./Parciales/MenuLateral.php" ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php require_once "./Parciales/Top.php" ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
         <!--<h1 class=" text-center">Registro de Usuario</h1>


          <h5 class="  text-gray-800 text-center d-lg-block">CENTRO UNIVERSITARIO TECNOLÓGICO</h5>-->



          <!-- Content Row -->
          <div class="row justify-content-center">
					<div class="col-lg-12 col-xl-12">
											<div class="card shadow mb-4 trans">
													<div class="card-header py-3">
														<!--<img src="img/logo.png" class="img-responsive" width="50%">-->
														<h3 class="m-0 font-weight-bold text-primary">Total artículos por clase</h3>
														
													</div>
													<div class="card-body">
													<form action="ReporteArticulosxClase.php" method="GET">
												  <input type="hidden"  id="IdSesion" name="IdSesion"></input>
													<div class="form-group">

													<div class="row">
																	
																					<div class="form-group col-sm-12 col-md-4">
																						<label for="idClase">Clase</label>
																						<select id="idClase" class="form-control" name="idClase">
																						<option value=""> --</option>
																			
										

																			<?php
																			$servername = "localhost";
																			$username = "root";
																			$password = "";
																			$dbname = "sistemadeinventario";

																				// Create connection
																			$conn = new mysqli($servername, $username, $password, $dbname);
																				// Check connection
																			if ($conn->connect_error) {
																			die("Connection failed: " . $conn->connect_error);
																		}

																		$sql = "SELECT IdClase, NombreClase from clases";
																		$result= $conn->query($sql);

																		if ($result->num_rows>0) {
																			while($row=$result->fetch_assoc()){
																				echo "<option value='".$row["IdClase"]."'>".$row["NombreClase"]."</option>";
																			}
																		} else {
																			echo "<option> --Tabla vacia--</option>";
																		}

																		$conn->close();
																			?>
																		
															
															


																	
																								</select>
																						</div>
																			<div class="form-group col-sm-12 col-md-4">
																				<label for="fechaInicial">Fecha inicial</label>
																				<input type="date" class="form-control" id="fechaInicial" name="fechaInicial" placeholder="Fecha Inicial">
																			</div>

																			<div class="form-group col-sm-12 col-md-4">
																				<label for="fechaFinal">Fecha final</label>
																				<input type="date" class="form-control" id="fechaFinal" name="fechaFinal" placeholder="Fecha Final">
																			</div>

																	</div>
																	<button type="submit" class="btn btn-primary ">Guardar</button>
																</form>
													</div>
											</div>
							</div>

          <div class="col-lg-12 col-xl-5">
						 <div class="card shadow mb-4 trans">
								<div class="card-header py-3">
				 					<!--<img src="img/logo.png" class="img-responsive" width="50%">-->
									 <?php
																			$servername = "localhost";
																			$username = "root";
																			$password = "";
																			$dbname = "sistemadeinventario";

																				// Create connection
																			$conn = new mysqli($servername, $username, $password, $dbname);
																				// Check connection
																			if ($conn->connect_error) {
																			die("Connection failed: " . $conn->connect_error);
																		}

																		$sql = "SELECT NombreClase from clases where IdClase=".$_GET["idClase"];
																		$result= $conn->query($sql);

																		if ($result->num_rows>0) {
																			while($row=$result->fetch_assoc()){
										
																				echo '<h3 class="m-0 font-weight-bold text-primary">Total artículos en '.$row["NombreClase"].'</h3>';
																			}
																		} else {
																			echo '<h3 class="m-0 font-weight-bold text-primary">Total artículos por clase</h3>';
																		}

																		$conn->close();
									?>


				 					
									 
				 				</div>
								 <div class="card-body">
								 <div class="table-responsive">
											<table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
												<thead>
													<tr>
														<th>Fecha</th>
                             <th>Clase</th>
														<th>Artículo</th>
														<th>Total </th>
													</tr>
												</thead>
												<tbody>
													<?php
														$servername = "localhost";
														$username = "root";
														$password = "";
														$dbname = "sistemadeinventario";

														// Create connection
														$conn = new mysqli($servername, $username, $password, $dbname);
														// Check connection
														if ($conn->connect_error) {
															die("Connection failed: " . $conn->connect_error);
														}
														

														$sql = "";

														if (isset($_GET["fechaInicial"])   and isset($_GET["fechaFinal"]) ){

																		$sql = "Select a.Fecha, 
																						c.NombreClase,
																						d.Descripcion,
																						(b.CantidadEntregada - b.CantidadDevolucion) Total 
																
																						From solicitud a
																						Inner join detalleSolicitud b
																						On a.IdSolicitud = b.IdSolicitud
																						And a.IdEstado != 3
																						INNER JOIN clases c
																						ON a.IdClase=c.IdClase
																						INNER JOIN articulo d
																						ON b.IdArticulo=d.IdArticulo
																						WHERE c.IdClase=".$_GET["idClase"]." and a.fecha between '".$_GET["fechaInicial"]."' and '".$_GET["fechaFinal"]."'
																						";
														}else{

																	$sql = "Select a.Fecha,
																					c.NombreClase,
																					d.Descripcion,
																					(b.CantidadEntregada - b.CantidadDevolucion) Total 
															
																					From solicitud a
																					Inner join detalleSolicitud b
																					On a.IdSolicitud = b.IdSolicitud
																					And a.IdEstado != 3
																					INNER JOIN clases c
																					ON a.IdClase=c.IdClase
																					INNER JOIN articulo d
																					ON b.IdArticulo=d.IdArticulo";

														}

														$result= $conn->query($sql);
														
														if ($result->num_rows>0) {
															while($row=$result->fetch_assoc()){
																
																print "<tr>
                                                                    <td>".$row['Fecha']."</td>".
                                                                    "<td>".$row['NombreClase']."</td>".
																	"<td>".$row['Descripcion']."</td>".
																	"<td>".$row['Total']."</td>".
																	"</tr>";
															}
														} else {
															echo "<option> --Tabla vacia--</option>";
														}

														$conn->close();

													?>
												</tbody>
											</table>                         
									 </div>
								 </div>
							 </div>
						 </div>


	 <?php

				$servername = "localhost";
				$username = "root";
				$password = "";
				$dbname = "sistemadeinventario";

				// Create connection
				$conn = new mysqli($servername, $username, $password, $dbname);
				// Check connection
				if ($conn->connect_error) {
					die("Connection failed: " . $conn->connect_error);
				}
				$sql = "";

														if (isset($_GET["fechaInicial"])   and isset($_GET["fechaFinal"]) ){

																		$sql = "Select a.Fecha, 
																						c.NombreClase,
																						d.Descripcion,
																						(b.CantidadEntregada - b.CantidadDevolucion) Total 
																
																						From solicitud a
																						Inner join detalleSolicitud b
																						On a.IdSolicitud = b.IdSolicitud
																						And a.IdEstado != 3
																						INNER JOIN clases c
																						ON a.IdClase=c.IdClase
																						INNER JOIN articulo d
																						ON b.IdArticulo=d.IdArticulo
																						GROUP BY d.Descripcion
																						WHERE c.IdClase=".$_GET["idClase"]." and a.fecha between '".$_GET["fechaInicial"]."' and '".$_GET["fechaFinal"]."'
																						";
														}else{

																	$sql = "Select a.Fecha,
																					c.NombreClase,
																					d.Descripcion,
																					(b.CantidadEntregada - b.CantidadDevolucion) Total 
															
																					From solicitud a
																					Inner join detalleSolicitud b
																					On a.IdSolicitud = b.IdSolicitud
																					And a.IdEstado != 3
																					INNER JOIN clases c
																					ON a.IdClase=c.IdClase
																					INNER JOIN articulo d
																					ON b.IdArticulo=d.IdArticulo
																					GROUP BY d.Descripcion";

														}

				$sql = "Select a.Fecha,
																		c.NombreClase,
																		d.Descripcion,
																		(b.CantidadEntregada - b.CantidadDevolucion) Total 
												
																		From solicitud a
																		Inner join detalleSolicitud b
																		On a.IdSolicitud = b.IdSolicitud
																		And a.IdEstado != 3
																		INNER JOIN clases c
																		ON a.IdClase=c.IdClase
																		INNER JOIN articulo d
																		ON b.IdArticulo=d.IdArticulo
																		GROUP BY d.Descripcion";
				$res= $conn->query($sql);
					/* if ($conn->query($sql) === TRUE) {
								
						} else {
								echo "Error: " . $sql . "<br>" . $conn->error;
						}*/
						
						$conn->close();

?>



    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Artículo', 'Total'],

          <?php

          while ($fila=$res->fetch_assoc()){
                echo "['".$fila["Descripcion"]."', ".$fila["Total"]."],";
          }
         // ['Work',     11],
          
          ?>
          
        ]);

        var options = {
          title: 'Gráfico de artículos por clase',
          is3D: true
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
		</script>
		
							<div class="col-lg-12 col-xl-6">
											<div class="card shadow mb-4 trans">
													<div class="card-header py-3">
														<!--<img src="img/logo.png" class="img-responsive" width="50%">-->
														<h3 class="m-0 font-weight-bold text-primary">Total artículos por clase</h3>
														
													</div>
													<div class="card-body p-0">
						
														<div id="piechart" class="col-lg-12 col-xl-12 col-md-12 col-sm-12 col-12"></div>
													</div>
											</div>
							</div>

      <!-- Donut Chart -->

    </div>

    <?php require_once "./Parciales/Scripts.php"?>
    <script type="text/javascript" src="../js/JSenviarIdSesion.js"></script>
  </body>

  </html>