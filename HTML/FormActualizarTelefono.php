<?php require_once "./Parciales/ValidacionDeSesion.php" ?>
<?php require_once "./Parciales/Encabezado.php" ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="../js/JSmensajeEnvio.js"></script>

<script type="text/javascript" src="../js/JSBusquedaTablaActualizarTelefono.js"></script>
<script type="text/javascript" src="../js/JSActualizarTelefono1.js"></script>
<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once "./Parciales/MenuLateral.php" ?>
          <!-- End of Sidebar -->

          <!-- Content Wrapper -->
          <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

              <!-- Topbar -->
              <?php require_once "./Parciales/Top.php" ?>
              <!-- End of Topbar -->
              
        <!-- Begin Page Content -->
        <div class="row justify-content-center">
        <?php
			if($permisoActivo==1){
      echo'
      
          <div class="col-lg-12 col-xl-7  ">
          <div class="progress">
						<div class="progress-bar bg-info progress-bar-striped progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 50%">Paso 1- Completado</div>
						<div class="progress-bar  progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 50%">Paso 2</div>
							
						</div>
      <div class="card shadow mb-4 trans">
      <div class="card-header py-3">
              <!--<img src="img/logo.png" class="img-responsive" width="50%">-->
              <h3 class="m-0 font-weight-bold text-primary">Actualizar teléfonos </h3>
              <h6 class="m-0 font-weight-bold text-primary">Teléfonos del usuario </h6>
          </div>
          <div class="card-body">

                  <div class="d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                    <div class="input-group">
                      <input type="text" class="form-control bg-light border-0 small" placeholder="Ingrese el número TH" aria-label="numeroTH" aria-describedby="basic-addon2" id="numeroTH" name="numeroTH">
                      <div class="input-group-append">
                        <button class="btn btn-primary" type="button" onclick="BuscarTodos();">
                          <i class="fas fa-search fa-sm"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                  <br>
                  <br>
                  <!-- Tabla-->
                  <div class="form-group col-sm-12 col-md-12" id="tabla">
                <table class="table" id="tabla">
                              <thead>
                                <tr>
                                  <th scope="col">IdTeléfono</th>
                                  <th scope="col">Número</th>
                                  <th scope="col">Tipo de teléfono</th>
                                  <th scope="col">Activo</th>
                                </tr>
                              </thead>
                              <tbody id="tablaBody">

                                    
                             </tbody>
                      </table>
                  </div>
                 <!-- Final Tabla-->

              

            </form>
          </div>
        
          <div class="card-header py-3">
              <!--<img src="img/logo.png" class="img-responsive" width="50%">-->
              <h6 class="m-0 font-weight-bold text-primary">Actualizar el teléfono</h6>
 
          </div>
          <div class="card-body">

                  <div class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                    <div class="input-group">
                      <input type="text" class="form-control bg-light border-0 small" placeholder="Ingrese ID del teléfono" aria-label="idTelefono" aria-describedby="basic-addon2" id="idTelefono1" name="idTelefono1">
                      <div class="input-group-append">
                        <button class="btn btn-primary" type="button" onclick="BuscarTel();">
                          <i class="fas fa-search fa-sm"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                  <br>
                  <br>
                  <form action="PHPActualizarTelefonoA.php" method="post">
                  <input type="hidden"  id="IdSesion" name="IdSesion"></input>
                    <div class="row">
                     <input type="hidden"  id="idTelefonoA" name="idTelefonoA"></input>


                      <div class="form-group col-sm-12 col-md-12">
                         <label for="numeroA">Número *</label>
                        <input type="text" class="form-control" id="numeroA" name="numeroA" placeholder="Número" required>
                      </div>
                     <div class="form-group col-sm-12 col-md-6">
                      <label for="idTipoUsuario">Tipo de teléfono *</label>
                      <select class="form-control" id="idTipoTelefonoA" name="idTipoTelefonoA" required>
                       <option value="--">--</option>
                     ';
                       $servername = "localhost";
                       $username = "root";
                       $password = "";
                       $dbname = "sistemadeinventario";

                    // Create connection
                       $conn = new mysqli($servername, $username, $password, $dbname);
                    // Check connection
                       if ($conn->connect_error) {
                         die("Connection failed: " . $conn->connect_error);
                       }

                       $sql = "SELECT idTipoTelefono, Descripcion from tipotelefono";
                       $result= $conn->query($sql);

                       if ($result->num_rows>0) {
                         while($row=$result->fetch_assoc()){
                          echo "<option value='".$row["idTipoTelefono"]."'>".$row["Descripcion"]."</option>";
                        }
                      } else {
                        echo "<option> --Tabla vacia--</option>";
                      }

                      $conn->close();

                      echo'
                    </select>
                  </div>

                  
               </div>



              <div class="form-group">
                <label for="activo">¿Está activo? *</label>       
                <select class="form-control" id="activoA" name="activoA" required>
                  <option value="--">--</option>
                  <option value="1">SI</option>
                  <option value="0">NO</option>
                </select>
              </div>

              <button type="submit" class="btn btn-primary ">Actualizar teléfono</button>

            </form>
          </div>
        </div>
        
        </div>';
      }else{
          echo '
          <div class="col-lg-12 col-xl-7 ">
            <div class="card shadow mb-4 trans">
              <div class="card-header py-3">
                <!--<img src="img/logo.png" class="img-responsive" width="50%">-->
                <h3 class="m-0 font-weight-bold text-primary">No tiene acceso</h3>
      
              </div>
            </div>
          </div>
          ';
      }
      ?>
      
      
      </div>

        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

 

  <?php require_once "./Parciales/Scripts.php"?>
  <script type="text/javascript" src="../js/JSenviarIdSesion.js"></script>
  <script type="text/javascript" src="../js/JSenviarNumeroTH.js"></script>
 

</body>

</html>