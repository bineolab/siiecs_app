<?php require_once "./Parciales/Encabezado.php" ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="../js/JSmensajeEnvio.js"></script>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once "./Parciales/MenuLateral.php" ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php require_once "./Parciales/Top.php" ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
         <!--<h1 class=" text-center">Registro de Usuario</h1>


          <h5 class="  text-gray-800 text-center d-lg-block">CENTRO UNIVERSITARIO TECNOLÓGICO</h5>-->



          <!-- Content Row -->
          <div class="row justify-content-center">


          <div class="col-lg-12 col-xl-7">
						 <div class="card shadow mb-4 trans">
								<div class="card-header py-3">
				 					<!--<img src="img/logo.png" class="img-responsive" width="50%">-->
				 					<h3 class="m-0 font-weight-bold text-primary">Cantidad de artículos existentes</h3>
                   <input type="hidden"  id="IdSesion" name="IdSesion"></input>
				 				</div>
								 <div class="card-body">
								 <div class="table-responsive">
											<table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
												<thead>
													<tr>
														<th>Id Artículo</th>
                            <th>Artículo</th>
														<th>Existencia</th>
														<th>Activo</th>
													</tr>
												</thead>
												<tbody>
													<?php
														$servername = "localhost";
														$username = "root";
														$password = "";
														$dbname = "sistemadeinventario";

														// Create connection
														$conn = new mysqli($servername, $username, $password, $dbname);
														// Check connection
														if ($conn->connect_error) {
															die("Connection failed: " . $conn->connect_error);
														}

														$sql = "select  x.IdArticulo, 
                            e.Descripcion Articulo, e.Activo,
                            (x.Cantidad - IFNULL(d.ent,0) + IFNULL(d.dev,0)) existencia
                           from 
                           (select  a.IdArticulo, 
                             sum(a.Cantidad) Cantidad 
                           from   inventariodetalle a
                           group by  a.IdArticulo) x left join 
                           (
                           select   c.IdArticulo, 
                                 sum(c.CantidadEntregada) ent , 
                                 sum(c.CantidadDevolucion) dev
                           from   solicitud b
                           inner join  detallesolicitud c
                           on    b.IdSolicitud = c.IdSolicitud
                           and   b.IdEstado != 3
                           group  by c.IdArticulo
                           ) d
                           on  x.IdArticulo = d.IdArticulo
                           inner join articulo e 
                           on x.idArticulo = e.idArticulo";
														$result= $conn->query($sql);
														$act="";
														if ($result->num_rows>0) {
															while($row=$result->fetch_assoc()){
																$act = ( $row['Activo']==1 ) ? "Si" : "No"; 
																print "<tr>
                                    <td>".$row['IdArticulo']."</td>".
                                    "<td>".$row['Articulo']."</td>".
																		"<td>".$row['existencia']."</td>".
																		"<td>".$act."</td>".
																	"</tr>";
															}
														} else {
															echo "<option> --Tabla vacia--</option>";
														}

														$conn->close();

													?>
												</tbody>
											</table>                         
									 </div>
								 </div>
							 </div>
						 </div>

      <!-- Donut Chart -->

    </div>

    <?php require_once "./Parciales/Scripts.php"?>
  
    <script type="text/javascript" src="../js/JSenviarIdSesion.js"></script>
  </body>

  </html>