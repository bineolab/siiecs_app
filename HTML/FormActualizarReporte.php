<?php require_once "./Parciales/Encabezado.php" ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="../js/JSmensajeEnvio.js"></script>
<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once "./Parciales/MenuLateral.php" ?>
          <!-- End of Sidebar -->

          <!-- Content Wrapper -->
          <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

              <!-- Topbar -->
              <?php require_once "./Parciales/Top.php" ?>
              <!-- End of Topbar -->

              <!-- Begin Page Content -->
              <div class="container-fluid">

                <!-- Page Heading -->
         <!--<h1 class=" text-center">Articulo</h1>
          
          
          <h5 class="  text-gray-800 text-center d-lg-block">CENTRO UNIVERSITARIO TECNOLÓGICO</h5>-->
          
          
          
          <!-- Content Row -->
          <div class="row justify-content-center">
      
            
                <div class="col-sm-12 col-md-10">
   <div class="card shadow mb-4 trans">
    <div class="card-header py-3">
        <!--<img src="img/logo.png" class="img-responsive" width="50%">-->
        <h3 class="m-0 font-weight-bold text-primary">Reporte Solicitud</h3>
    </div>
    
    <div class="card-body">
       <div class="table-responsive">
        <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>IdArticulo</th>
                    <th>CantidadSolicitada</th>
                    <th>CantidadDevolucion</th>
                    
                </tr>
            </thead>
            <tbody>
                <?php
                $servername = "localhost";
                $username = "root";
                $password = "";
                $dbname = "sistemadeinventario";

                                                        // Create connection
                $conn = new mysqli($servername, $username, $password, $dbname);
                                                        // Check connection
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }

                $sql = "
				SELECT A.IdDetalleSolicitud, B.IdSolicitud, A.IdArticulo, C.Descripcion , sum(A.CantidadSolicitada) as cantidadTotal, sum(A.CantidadDevolucion) as cantidadDevTotal FROM detallesolicitud A INNER JOIN solicitud B ON B.IdSolicitud = B.IdSolicitud INNER JOIN articulo C ON C.IdArticulo = A.IdArticulo GROUP by C.Descripcion";
				
                $result= $conn->query($sql);
                $act="";
                if ($result->num_rows>0) {
                    while($row=$result->fetch_assoc()){
                  
                        print "<tr>
                        <td>".$row['Descripcion']."</td>".
                        "<td>".$row['cantidadTotal']."</td>".
                        "<td>".$row['cantidadDevTotal']."</td>".
                        "<td>".$act."</td>".
                        "</tr>";
                    }
                } else {
                    echo "<option> --Tabla vacia--</option>";
                }

                $conn->close();

                ?>
            </tbody>
        </table>                         
    </div>
</div>
</div>

    <!-- Donut Chart -->
    
  </div>

  <?php require_once "./Parciales/Scripts.php"?>
  <script type="text/javascript" src="../js/JSenviarIdSesion.js"></script>
  <script type="text/javascript" src="../js/JSReporteInsumo.js"></script>
 
</body>

</html>