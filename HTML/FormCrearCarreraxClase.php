<?php require_once "./Parciales/ValidacionDeSesion.php" ?>
<?php require_once "./Parciales/Encabezado.php" ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="../js/JSmensajeEnvio.js"></script>
<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once "./Parciales/MenuLateral.php" ?>
          <!-- End of Sidebar -->

          <!-- Content Wrapper -->
          <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

              <!-- Topbar -->
              <?php require_once "./Parciales/Top.php" ?>
              <!-- End of Topbar -->

              <!-- Begin Page Content -->
              <div class="container-fluid">


                <!-- Page Heading -->
         <!--<h1 class=" text-center">Registro de Usuario</h1>


          <h5 class="  text-gray-800 text-center d-lg-block">CENTRO UNIVERSITARIO TECNOLÓGICO</h5>-->



          <!-- Content Row -->
          <div class="row justify-content-center">
          <?php
			if($permisoActivo==1){
			echo '

            <div class="col-lg-12 col-xl-7  ">
              <div class="card shadow mb-4 trans">
                <div class="card-header py-3">
                  <!--<img src="img/logo.png" class="img-responsive" width="50%">-->
                  <h3 class="m-0 font-weight-bold text-primary">Agregar una clase a una carrera existente</h3>
                </div>


                <!--Formulario-->
                <div class="card-body">
                  <form action="PHPCrearCarreraxClase.php" method="POST">
				    <input type="hidden"  id="IdSesion" name="IdSesion"></input>
                  <div class="form-group">

                  <div class="row">




                      <div class="form-group col-sm-12 col-md-6">
                <label for="idCarrera">Carrera *</label>
                <select class="form-control" id="idCarrera" name="idCarrera" onchange ="Cambiar();" required>
                <option value="--">--</option>
                 ';
                  $servername = "localhost";
                  $username = "root";
                  $password = "";
                  $dbname = "sistemadeinventario";

                    // Create connection
                  $conn = new mysqli($servername, $username, $password, $dbname);
                    // Check connection
                  if ($conn->connect_error) {
                   die("Connection failed: " . $conn->connect_error);
                 }

                 $sql = "SELECT IdCarrera, NombreCarrera from carreras";
                 $result= $conn->query($sql);

                 if ($result->num_rows>0) {
                   while($row=$result->fetch_assoc()){

                    if ($_GET["idCarrera"] == $row["IdCarrera"] ){
                      echo "<option value='".$row["IdCarrera"]."'  selected>".$row["NombreCarrera"]."</option>";
                    }else{
                      echo "<option value='".$row["IdCarrera"]."'>".$row["NombreCarrera"]."</option>";
                    }
                  }
                } else {
                  echo "<option> --Tabla vacia--</option>";
                }

                $conn->close();

              echo'
              </select>
            </div>


            <div class="form-group col-sm-12 col-md-6">
                      <label for="idClase">Clase *</label>
                      <select class="form-control" id="idClase" name="idClase" required>
                       <option value="--">--</option>
                       ';
                       header("Content-Type:text/html;charset=utf8");
                       mysql_query("SET NAMES 'utf8'");
                       $servername = "localhost";
                       $username = "root";
                       $password = "";
                       $dbname = "sistemadeinventario";

                    // Create connection
                       $conn = new mysqli($servername, $username, $password, $dbname);
                    // Check connection
                       if ($conn->connect_error) {
                         die("Connection failed: " . $conn->connect_error);
                       }

                       $sql = "SELECT   IdClase, 
                                        NombreClase 
                                from    clases a 
                                where   not exists (select * 
                                                        from carreraxclase b 
                                                        where a.idClase = b.IdClase 
                                                        and b.idCarrera = ".$_GET["idCarrera"].") 
                                ORDER BY NombreClase asc";
                       $result= $conn->query($sql);

                       if ($result->num_rows>0) {
                         while($row=$result->fetch_assoc()){
                          echo "<option value='".$row["IdClase"]."'>".$row["NombreClase"]."</option>";
                        }
                      } else {
                        echo "<option> --Tabla vacia--</option>";
                      }

                      $conn->close();

                echo'
                    </select>
                  </div>

                    


                    </div>

            <button type="submit" class="btn btn-primary ">Guardar</button>

          </form>
        </div>
      </div>';
    }else{
        echo '
        <div class="col-lg-12 col-xl-7 ">
          <div class="card shadow mb-4 trans">
            <div class="card-header py-3">
              <!--<img src="img/logo.png" class="img-responsive" width="50%">-->
              <h3 class="m-0 font-weight-bold text-primary">No tiene acceso</h3>
    
            </div>
          </div>
        </div>
        ';
    }
    ?>


    </div>

    <!-- Donut Chart -->

  </div>

  <?php require_once "./Parciales/Scripts.php"?>
    <script type="text/javascript" src="../js/JSenviarIdSesion.js"></script>

    <script >
      function Cambiar(){
        
          $.urlParam = function(name){
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            if (results==null) {
              return null;
            }
            return decodeURI(results[1]) || 0;
          }

          var idsesion =$.urlParam('IdSesion');

          var url = "FormCrearCarreraxClase.php?IdSesion="+idsesion;
          
          
          var valor = document.getElementById("idCarrera").selectedIndex;
          url = url + "&idCarrera="+valor;

          window.location.replace(url);

        
      }
    </script>

</body>

</html>
