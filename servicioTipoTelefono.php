<?php

header("Content-type: text/xml"); 
$servername = "localhost";
$username = "root"; 
$password = "";
$database = "sistemadeinventario";
$conn = new mysqli($servername, $username, $password,$database);

if ($conn->connect_error) {
    die("Conexión Fallida: " . $conn->connect_error);
} 

$sql = "SELECT * FROM tipotelefono";
$result = $conn->query($sql); 

$xml = "<?xml version=\"1.0\"?>\n"; 
$xml .= "<tiposTelefonos>\n"; 
if($result->num_rows > 0){
    while($row = $result->fetch_assoc()){
        $xml .= "\t<tipo>\n"; 
		$xml .= "\t\t<id>" . $row['IdTipoTelefono'] . "</id>\n"; 
        $xml .= "\t\t<descripcion>" . $row['Descripcion'] . "</descripcion>\n";
        $xml .= "\t\t<activo>" . $row['Activo'] . "</activo>\n";
        $xml .= "\t</tipo>\n"; 
    }
}
$xml .= "</tiposTelefonos>"; 
print $xml;